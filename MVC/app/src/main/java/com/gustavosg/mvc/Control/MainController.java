package com.gustavosg.mvc.Control;

import com.gustavosg.mvc.BD.BD;
import com.gustavosg.mvc.Model.Carro;

import java.util.ArrayList;

/**
 * Projeto: MVC
 * Criado por Gustavo em 06/02/2017.
 */
public class MainController {

    private CarroController carroController;

    public MainController(BD bd) {

        carroController = new CarroController(bd);
    }

    public String inserirCarro(String nome, String marca) {
        String saida = "";
        if (carroController.inserirCarro(new Carro(nome, marca)) != -1) {
            saida = "Carro inserido com sucesso!";
        } else {
            saida = "ERRO! Tente novamente inserir o carro!";
        }
        return saida;
    }

    public boolean existeCarroCadastrado() {
        return carroController.existeCarroCadastrado();
    }

    public ArrayList<Carro> carrosCadastrados() {
        return carroController.carrosCadastrados();
    }
}
