package com.gustavosg.mvc.Model;

/**
 * Projeto: MVC
 * Criado por Gustavo em 06/02/2017.
 */
public class Carro {
    private String nome;
    private String marca;

    public Carro() {

    }

    public Carro(String nome, String marca) {
        this.nome = nome;
        this.marca = marca;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
