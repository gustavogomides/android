package com.gustavosg.mvc.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gustavosg.mvc.Model.Carro;

import java.util.ArrayList;

public class BD {
    private SQLiteDatabase bd;
    private BDCore auxBd;

    public BD(Context context) {
        auxBd = new BDCore(context);
        bd = auxBd.getWritableDatabase();
    }

    public void desconectar() {
        SQLiteDatabase db = auxBd.getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    public long inserirCarro(Carro carro) {
        ContentValues valores = new ContentValues();
        valores.put("nome", carro.getNome());
        valores.put("marca", carro.getMarca());

        return bd.insert("carro", null, valores);
    }

    public ArrayList<Carro> carrosCadastrados() {
        ArrayList<Carro> arrayListCarros = new ArrayList<>();

        String sql = "SELECT * from carro order by (nome)";
        Cursor cursor = bd.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            return null;
        } else if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                arrayListCarros.add(new Carro(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arrayListCarros;
    }

    public boolean existeCarroCadastrado() {
        boolean existe;

        String sql = "SELECT * from carro";
        Cursor cursor = bd.rawQuery(sql, null);

        existe = cursor.getCount() != 0;

        cursor.close();
        return existe;
    }
/*

    public boolean deletarPoupanca(String nome) {
        String table = "poupanca";
        String where = "nome = '" + nome + "'";
        Log.i("NOME", nome);
        return bd.delete(table, where, null) != 0;
    }

    public long atualizarPoupanca(String nome, double valor) {
        ContentValues valores = new ContentValues();
        valores.put("valor", valor);
        return bd.update("poupanca", valores, "nome=?", new String[]{nome});
    }
    */
}

