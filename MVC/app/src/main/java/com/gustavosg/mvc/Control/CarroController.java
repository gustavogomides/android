package com.gustavosg.mvc.Control;

import com.gustavosg.mvc.BD.BD;
import com.gustavosg.mvc.Model.Carro;

import java.util.ArrayList;

/**
 * Projeto: MVC
 * Criado por Gustavo em 06/02/2017.
 */
public class CarroController {

    private BD bd;

    public CarroController(BD bd) {
        this.bd = bd;
    }

    public long inserirCarro(Carro carro) {
        return bd.inserirCarro(carro);
    }

    public boolean existeCarroCadastrado() {
        return bd.existeCarroCadastrado();
    }

    public ArrayList<Carro> carrosCadastrados() {
        return bd.carrosCadastrados();
    }
}
