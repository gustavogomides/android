package gustavosg.com.diferencahoras;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private EditText etHoraInicio, etMinutoInicio;
    private EditText etHoraFim, etMinutoFim;
    private Button calcular;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etHoraInicio = (EditText) findViewById(R.id.etHoraInicio);
        etMinutoInicio = (EditText) findViewById(R.id.etMinutoInicio);
        etHoraFim = (EditText) findViewById(R.id.etHoraFim);
        etMinutoFim = (EditText) findViewById(R.id.etMinutoFim);
        calcular = (Button) findViewById(R.id.calcular);
        resultado = (TextView) findViewById(R.id.resultado);
    }


    @Override
    protected void onStart() {
        super.onStart();

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etHoraInicio.getText().toString().equals("") || etHoraInicio.getText() == null
                        || etMinutoInicio.getText().toString().equals("") || etMinutoInicio.getText() == null
                        || etHoraFim.getText().toString().equals("") || etHoraFim.getText() == null
                        || etMinutoFim.getText().toString().equals("") || etMinutoFim.getText() == null) {
                    Toast.makeText(MainActivity.this, "Digite um horário!", Toast.LENGTH_SHORT).show();
                } else {
                    calcularHoras(Integer.parseInt(etHoraInicio.getText().toString()),
                            Integer.parseInt(etMinutoInicio.getText().toString()),
                            Integer.parseInt(etHoraFim.getText().toString()),
                            Integer.parseInt(etMinutoFim.getText().toString()));
                }
            }
        });
    }

    private void calcularHoras(int hi, int mi, int hf, int mf) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.set(Calendar.HOUR, hi);
        c1.set(Calendar.MINUTE, mi);

        c2.set(Calendar.HOUR, hf);
        c2.set(Calendar.MINUTE, mf);

        //diferenca em horas e minutos
        long diff = c2.getTimeInMillis() - c1.getTimeInMillis();
        long hours = (60 * 60 * 1000);
        long difHoras = diff / hours;
        long difMinutos = (diff % hours) / (60 * 1000);
        String saida = "Hora Início = " + hi + " : " + mi + "\n\n"
                + "Hora Fim = " + hf + " : " + mf + "\n\n"
                + "Diferença de Horário = " + difHoras + " : " + difMinutos;
        resultado.setText(saida);
    }
}

