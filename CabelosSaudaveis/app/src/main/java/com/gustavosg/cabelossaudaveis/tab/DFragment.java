package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;

public class DFragment extends Fragment implements View.OnClickListener {

    public static final String comp1D = "componente1DD";
    public static final String comp2D = "componente2DD";
    public static final String comp3D = "componente3DD";
    public static final String comp4D = "componente4DD";
    public static final String comp5D = "componente5DD";
    public static final String comp6D = "componente6DD";
    public static final String comp7D = "componente7DD";
    public static final String comp8D = "componente8DD";
    public static final String comp9D = "componente9DD";
    public static final String comp10D = "componente10DD";
    public static final String comp11D = "componente11DD";
    public static final String comp12D = "componente12DD";
    public static final String comp13D = "componente13DD";
    public static final String comp14D = "componente14DD";
    public static final String comp15D = "componente15DD";
    public static final String comp16D = "componente16DD";
    public static final String comp17D = "componente17DD";
    public static final String comp18D = "componente18DD";
    public static final String comp19D = "componente19DD";
    public static final String comp20D = "componente20DD";
    public static final String comp21D = "componente21DD";

    public CheckBox componente1D;
    public CheckBox componente2D;
    public CheckBox componente3D;
    public CheckBox componente4D;
    public CheckBox componente5D;
    public CheckBox componente6D;
    public CheckBox componente7D;
    public CheckBox componente8D;
    public CheckBox componente9D;
    public CheckBox componente10D;
    public CheckBox componente11D;
    public CheckBox componente12D;
    public CheckBox componente13D;
    public CheckBox componente14D;
    public CheckBox componente15D;
    public CheckBox componente16D;
    public CheckBox componente17D;
    public CheckBox componente18D;
    public CheckBox componente19D;
    public CheckBox componente20D;
    public CheckBox componente21D;

    private View view;

    private SharedPreferences spc1D;
    private SharedPreferences spc2D;
    private SharedPreferences spc3D;
    private SharedPreferences spc4D;
    private SharedPreferences spc5D;
    private SharedPreferences spc6D;
    private SharedPreferences spc7D;
    private SharedPreferences spc8D;
    private SharedPreferences spc9D;
    private SharedPreferences spc10D;
    private SharedPreferences spc11D;
    private SharedPreferences spc12D;
    private SharedPreferences spc13D;
    private SharedPreferences spc14D;
    private SharedPreferences spc15D;
    private SharedPreferences spc16D;
    private SharedPreferences spc17D;
    private SharedPreferences spc18D;
    private SharedPreferences spc19D;
    private SharedPreferences spc20D;
    private SharedPreferences spc21D;

    private SharedPreferences.Editor ec1D;
    private SharedPreferences.Editor ec2D;
    private SharedPreferences.Editor ec3D;
    private SharedPreferences.Editor ec4D;
    private SharedPreferences.Editor ec5D;
    private SharedPreferences.Editor ec6D;
    private SharedPreferences.Editor ec7D;
    private SharedPreferences.Editor ec8D;
    private SharedPreferences.Editor ec9D;
    private SharedPreferences.Editor ec10D;
    private SharedPreferences.Editor ec11D;
    private SharedPreferences.Editor ec12D;
    private SharedPreferences.Editor ec13D;
    private SharedPreferences.Editor ec14D;
    private SharedPreferences.Editor ec15D;
    private SharedPreferences.Editor ec16D;
    private SharedPreferences.Editor ec17D;
    private SharedPreferences.Editor ec18D;
    private SharedPreferences.Editor ec19D;
    private SharedPreferences.Editor ec20D;
    private SharedPreferences.Editor ec21D;

    private boolean clicou = false;

    public DFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_d,
                container, false);

        setarCheckBox();
        return view;
    }

    private void setarSP() {
        spc1D = getActivity().getSharedPreferences(comp1D, 0);
        spc2D = getActivity().getSharedPreferences(comp2D, 0);
        spc3D = getActivity().getSharedPreferences(comp3D, 0);
        spc4D = getActivity().getSharedPreferences(comp4D, 0);
        spc5D = getActivity().getSharedPreferences(comp5D, 0);
        spc6D = getActivity().getSharedPreferences(comp6D, 0);
        spc7D = getActivity().getSharedPreferences(comp7D, 0);
        spc8D = getActivity().getSharedPreferences(comp8D, 0);
        spc9D = getActivity().getSharedPreferences(comp9D, 0);
        spc10D = getActivity().getSharedPreferences(comp10D, 0);
        spc11D = getActivity().getSharedPreferences(comp11D, 0);
        spc12D = getActivity().getSharedPreferences(comp12D, 0);
        spc13D = getActivity().getSharedPreferences(comp13D, 0);
        spc14D = getActivity().getSharedPreferences(comp14D, 0);
        spc15D = getActivity().getSharedPreferences(comp15D, 0);
        spc16D = getActivity().getSharedPreferences(comp16D, 0);
        spc17D = getActivity().getSharedPreferences(comp17D, 0);
        spc18D = getActivity().getSharedPreferences(comp18D, 0);
        spc19D = getActivity().getSharedPreferences(comp19D, 0);
        spc20D = getActivity().getSharedPreferences(comp20D, 0);
        spc21D = getActivity().getSharedPreferences(comp21D, 0);

        ec1D = spc1D.edit();
        ec2D = spc2D.edit();
        ec3D = spc3D.edit();
        ec4D = spc4D.edit();
        ec5D = spc5D.edit();
        ec6D = spc6D.edit();
        ec7D = spc7D.edit();
        ec8D = spc8D.edit();
        ec9D = spc9D.edit();
        ec10D = spc10D.edit();
        ec11D = spc11D.edit();
        ec12D = spc12D.edit();
        ec13D = spc13D.edit();
        ec14D = spc14D.edit();
        ec15D = spc15D.edit();
        ec16D = spc16D.edit();
        ec17D = spc17D.edit();
        ec18D = spc18D.edit();
        ec19D = spc19D.edit();
        ec20D = spc20D.edit();
        ec21D = spc21D.edit();
    }

    private void setarCheckBox() {
        componente1D = (CheckBox) view.findViewById(R.id.componente1_D);
        componente2D = (CheckBox) view.findViewById(R.id.componente2_D);
        componente3D = (CheckBox) view.findViewById(R.id.componente3_D);
        componente4D = (CheckBox) view.findViewById(R.id.componente4_D);
        componente5D = (CheckBox) view.findViewById(R.id.componente5_D);
        componente6D = (CheckBox) view.findViewById(R.id.componente6_D);
        componente7D = (CheckBox) view.findViewById(R.id.componente7_D);
        componente8D = (CheckBox) view.findViewById(R.id.componente8_D);
        componente9D = (CheckBox) view.findViewById(R.id.componente9_D);
        componente10D = (CheckBox) view.findViewById(R.id.componente10_D);
        componente11D = (CheckBox) view.findViewById(R.id.componente11_D);
        componente12D = (CheckBox) view.findViewById(R.id.componente12_D);
        componente13D = (CheckBox) view.findViewById(R.id.componente13_D);
        componente14D = (CheckBox) view.findViewById(R.id.componente14_D);
        componente15D = (CheckBox) view.findViewById(R.id.componente15_D);
        componente16D = (CheckBox) view.findViewById(R.id.componente16_D);
        componente17D = (CheckBox) view.findViewById(R.id.componente17_D);
        componente18D = (CheckBox) view.findViewById(R.id.componente18_D);
        componente19D = (CheckBox) view.findViewById(R.id.componente19_D);
        componente20D = (CheckBox) view.findViewById(R.id.componente20_D);
        componente21D = (CheckBox) view.findViewById(R.id.componente21_D);

    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1D.setOnClickListener(this);
        componente2D.setOnClickListener(this);
        componente3D.setOnClickListener(this);
        componente4D.setOnClickListener(this);
        componente5D.setOnClickListener(this);
        componente6D.setOnClickListener(this);
        componente7D.setOnClickListener(this);
        componente8D.setOnClickListener(this);
        componente9D.setOnClickListener(this);
        componente10D.setOnClickListener(this);
        componente11D.setOnClickListener(this);
        componente12D.setOnClickListener(this);
        componente13D.setOnClickListener(this);
        componente14D.setOnClickListener(this);
        componente15D.setOnClickListener(this);
        componente16D.setOnClickListener(this);
        componente17D.setOnClickListener(this);
        componente18D.setOnClickListener(this);
        componente19D.setOnClickListener(this);
        componente20D.setOnClickListener(this);
        componente21D.setOnClickListener(this);

        if (!clicou) {
            ec1D.putBoolean("estado1DD", false);
            ec1D.commit();

            ec2D.putBoolean("estado2DD", false);
            ec2D.commit();

            ec3D.putBoolean("estado3DD", false);
            ec3D.commit();

            ec4D.putBoolean("estado4DD", false);
            ec4D.commit();

            ec5D.putBoolean("estado5DD", false);
            ec5D.commit();

            ec6D.putBoolean("estado6DD", false);
            ec6D.commit();

            ec7D.putBoolean("estado7DD", false);
            ec7D.commit();

            ec8D.putBoolean("estado8DD", false);
            ec8D.commit();

            ec9D.putBoolean("estado9DD", false);
            ec9D.commit();

            ec10D.putBoolean("estado10DD", false);
            ec10D.commit();

            ec11D.putBoolean("estado11DD", false);
            ec11D.commit();

            ec12D.putBoolean("estado12DD", false);
            ec12D.commit();

            ec13D.putBoolean("estado13DD", false);
            ec13D.commit();

            ec14D.putBoolean("estado14DD", false);
            ec14D.commit();

            ec15D.putBoolean("estado15DD", false);
            ec15D.commit();

            ec16D.putBoolean("estado16DD", false);
            ec16D.commit();

            ec17D.putBoolean("estado17DD", false);
            ec17D.commit();

            ec18D.putBoolean("estado18DD", false);
            ec18D.commit();

            ec19D.putBoolean("estado19DD", false);
            ec19D.commit();

            ec20D.putBoolean("estado20DD", false);
            ec20D.commit();

            ec21D.putBoolean("estado21DD", false);
            ec21D.commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_D:
                ec1D.putBoolean("estado1DD", componente1D.isChecked());
                ec1D.commit();
                break;
            case R.id.componente2_D:
                ec2D.putBoolean("estado2DD", componente2D.isChecked());
                ec2D.commit();
                break;
            case R.id.componente3_D:
                ec3D.putBoolean("estado3DD", componente3D.isChecked());
                ec3D.commit();
                break;
            case R.id.componente4_D:
                ec4D.putBoolean("estado4DD", componente4D.isChecked());
                ec4D.commit();
                break;
            case R.id.componente5_D:
                ec5D.putBoolean("estado5DD", componente5D.isChecked());
                ec5D.commit();
                break;
            case R.id.componente6_D:
                ec6D.putBoolean("estado6DD", componente6D.isChecked());
                ec6D.commit();
                break;
            case R.id.componente7_D:
                ec7D.putBoolean("estado7DD", componente7D.isChecked());
                ec7D.commit();
                break;
            case R.id.componente8_D:
                ec8D.putBoolean("estado8DD", componente8D.isChecked());
                ec8D.commit();
                break;
            case R.id.componente9_D:
                ec9D.putBoolean("estado9DD", componente9D.isChecked());
                ec9D.commit();
                break;

            case R.id.componente10_D:
                ec10D.putBoolean("estado10DD", componente10D.isChecked());
                ec10D.commit();
                break;

            case R.id.componente11_D:
                ec11D.putBoolean("estado11DD", componente11D.isChecked());
                ec11D.commit();
                break;

            case R.id.componente12_D:
                ec12D.putBoolean("estado12DD", componente12D.isChecked());
                ec12D.commit();
                break;

            case R.id.componente13_D:
                ec13D.putBoolean("estado13DD", componente13D.isChecked());
                ec13D.commit();
                break;

            case R.id.componente14_D:
                ec14D.putBoolean("estado14DD", componente14D.isChecked());
                ec14D.commit();
                break;

            case R.id.componente15_D:
                ec15D.putBoolean("estado15DD", componente15D.isChecked());
                ec15D.commit();
                break;

            case R.id.componente16_D:
                ec16D.putBoolean("estado16DD", componente16D.isChecked());
                ec16D.commit();
                break;

            case R.id.componente17_D:
                ec17D.putBoolean("estado17DD", componente17D.isChecked());
                ec17D.commit();
                break;

            case R.id.componente18_D:
                ec18D.putBoolean("estado18DD", componente18D.isChecked());
                ec18D.commit();
                break;

            case R.id.componente19_D:
                ec19D.putBoolean("estado19DD", componente19D.isChecked());
                ec19D.commit();
                break;

            case R.id.componente20_D:
                ec20D.putBoolean("estado20DD", componente20D.isChecked());
                ec20D.commit();
                break;

            case R.id.componente21_D:
                ec21D.putBoolean("estado21DD", componente21D.isChecked());
                ec21D.commit();
                break;
        }
    }

}