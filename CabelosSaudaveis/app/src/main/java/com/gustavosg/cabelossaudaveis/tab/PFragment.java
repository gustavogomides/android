package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class PFragment extends Fragment implements View.OnClickListener {
    public static final String comp1P = "componente1P";
    public static final String comp2P = "componente2P";
    public static final String comp3P = "componente3P";
    public static final String comp4P = "componente4P";
    public static final String comp5P = "componente5P";
    public static final String comp6P = "componente6P";
    public static final String comp7P = "componente7P";
    public static final String comp8P = "componente8P";
    public static final String comp9P = "componente9P";
    public static final String comp10P = "componente10P";
    public static final String comp11P = "componente11P";
    public static final String comp12P = "componente12P";
    public static final String comp13P = "componente13P";
    public static final String comp14P = "componente14P";
    public static final String comp15P = "componente15P";
    public static final String comp16P = "componente16P";
    public static final String comp17P = "componente17P";
    public static final String comp18P = "componente18P";
    public static final String comp19P = "componente19P";
    public static final String comp20P = "componente20P";
    public static final String comp21P = "componente21P";
    public static final String comp22P = "componente22P";
    public static final String comp23P = "componente23P";
    public static final String comp24P = "componente24P";
    public static final String comp25P = "componente25P";
    public static final String comp26P = "componente26P";
    public static final String comp27P = "componente27P";
    public static final String comp28P = "componente28P";
    public static final String comp29P = "componente29P";
    public static final String comp30P = "componente30P";
    public static final String comp31P = "componente31P";
    public static final String comp32P = "componente32P";
    public static final String comp33P = "componente33P";
    public static final String comp34P = "componente34P";
    public static final String comp35P = "componente35P";
    public static final String comp36P = "componente36P";
    public static final String comp37P = "componente37P";
    public static final String comp38P = "componente38P";
    public static final String comp39P = "componente39P";
    public static final String comp40P = "componente40P";

    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;
    public CheckBox componente5;
    public CheckBox componente6;
    public CheckBox componente7;
    public CheckBox componente8;
    public CheckBox componente9;
    public CheckBox componente10;
    public CheckBox componente11;
    public CheckBox componente12;
    public CheckBox componente13;
    public CheckBox componente14;
    public CheckBox componente15;
    public CheckBox componente16;
    public CheckBox componente17;
    public CheckBox componente18;
    public CheckBox componente19;
    public CheckBox componente20;
    public CheckBox componente21;
    public CheckBox componente22;
    public CheckBox componente23;
    public CheckBox componente24;
    public CheckBox componente25;
    public CheckBox componente26;
    public CheckBox componente27;
    public CheckBox componente28;
    public CheckBox componente29;
    public CheckBox componente30;
    public CheckBox componente31;
    public CheckBox componente32;
    public CheckBox componente33;
    public CheckBox componente34;
    public CheckBox componente35;
    public CheckBox componente36;
    public CheckBox componente37;
    public CheckBox componente38;
    public CheckBox componente39;
    public CheckBox componente40;

    private SharedPreferences spc1P;
    private SharedPreferences spc2P;
    private SharedPreferences spc3P;
    private SharedPreferences spc4P;
    private SharedPreferences spc5P;
    private SharedPreferences spc6P;
    private SharedPreferences spc7P;
    private SharedPreferences spc8P;
    private SharedPreferences spc9P;
    private SharedPreferences spc10P;
    private SharedPreferences spc11P;
    private SharedPreferences spc12P;
    private SharedPreferences spc13P;
    private SharedPreferences spc14P;
    private SharedPreferences spc15P;
    private SharedPreferences spc16P;
    private SharedPreferences spc17P;
    private SharedPreferences spc18P;
    private SharedPreferences spc19P;
    private SharedPreferences spc20P;
    private SharedPreferences spc21P;
    private SharedPreferences spc22P;
    private SharedPreferences spc23P;
    private SharedPreferences spc24P;
    private SharedPreferences spc25P;
    private SharedPreferences spc26P;
    private SharedPreferences spc27P;
    private SharedPreferences spc28P;
    private SharedPreferences spc29P;
    private SharedPreferences spc30P;
    private SharedPreferences spc31P;
    private SharedPreferences spc32P;
    private SharedPreferences spc33P;
    private SharedPreferences spc34P;
    private SharedPreferences spc35P;
    private SharedPreferences spc36P;
    private SharedPreferences spc37P;
    private SharedPreferences spc38P;
    private SharedPreferences spc39P;
    private SharedPreferences spc40P;

    private SharedPreferences.Editor ec1P;
    private SharedPreferences.Editor ec2P;
    private SharedPreferences.Editor ec3P;
    private SharedPreferences.Editor ec4P;
    private SharedPreferences.Editor ec5P;
    private SharedPreferences.Editor ec6P;
    private SharedPreferences.Editor ec7P;
    private SharedPreferences.Editor ec8P;
    private SharedPreferences.Editor ec9P;
    private SharedPreferences.Editor ec10P;
    private SharedPreferences.Editor ec11P;
    private SharedPreferences.Editor ec12P;
    private SharedPreferences.Editor ec13P;
    private SharedPreferences.Editor ec14P;
    private SharedPreferences.Editor ec15P;
    private SharedPreferences.Editor ec16P;
    private SharedPreferences.Editor ec17P;
    private SharedPreferences.Editor ec18P;
    private SharedPreferences.Editor ec19P;
    private SharedPreferences.Editor ec20P;
    private SharedPreferences.Editor ec21P;
    private SharedPreferences.Editor ec22P;
    private SharedPreferences.Editor ec23P;
    private SharedPreferences.Editor ec24P;
    private SharedPreferences.Editor ec25P;
    private SharedPreferences.Editor ec26P;
    private SharedPreferences.Editor ec27P;
    private SharedPreferences.Editor ec28P;
    private SharedPreferences.Editor ec29P;
    private SharedPreferences.Editor ec30P;
    private SharedPreferences.Editor ec31P;
    private SharedPreferences.Editor ec32P;
    private SharedPreferences.Editor ec33P;
    private SharedPreferences.Editor ec34P;
    private SharedPreferences.Editor ec35P;
    private SharedPreferences.Editor ec36P;
    private SharedPreferences.Editor ec37P;
    private SharedPreferences.Editor ec38P;
    private SharedPreferences.Editor ec39P;
    private SharedPreferences.Editor ec40P;

    private boolean clicou = false;

    private View view;

    public PFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_p,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_P);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_P);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_P);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_P);
        componente5 = (CheckBox) view.findViewById(R.id.componente5_P);
        componente6 = (CheckBox) view.findViewById(R.id.componente6_P);
        componente7 = (CheckBox) view.findViewById(R.id.componente7_P);
        componente8 = (CheckBox) view.findViewById(R.id.componente8_P);
        componente9 = (CheckBox) view.findViewById(R.id.componente9_P);
        componente10 = (CheckBox) view.findViewById(R.id.componente10_P);
        componente11 = (CheckBox) view.findViewById(R.id.componente11_P);
        componente12 = (CheckBox) view.findViewById(R.id.componente12_P);
        componente13 = (CheckBox) view.findViewById(R.id.componente13_P);
        componente14 = (CheckBox) view.findViewById(R.id.componente14_P);
        componente15 = (CheckBox) view.findViewById(R.id.componente15_P);
        componente16 = (CheckBox) view.findViewById(R.id.componente16_P);
        componente17 = (CheckBox) view.findViewById(R.id.componente17_P);
        componente18 = (CheckBox) view.findViewById(R.id.componente18_P);
        componente19 = (CheckBox) view.findViewById(R.id.componente19_P);
        componente20 = (CheckBox) view.findViewById(R.id.componente20_P);
        componente21 = (CheckBox) view.findViewById(R.id.componente21_P);
        componente22 = (CheckBox) view.findViewById(R.id.componente22_P);
        componente23 = (CheckBox) view.findViewById(R.id.componente23_P);
        componente24 = (CheckBox) view.findViewById(R.id.componente24_P);
        componente25 = (CheckBox) view.findViewById(R.id.componente25_P);
        componente26 = (CheckBox) view.findViewById(R.id.componente26_P);
        componente27 = (CheckBox) view.findViewById(R.id.componente27_P);
        componente28 = (CheckBox) view.findViewById(R.id.componente28_P);
        componente29 = (CheckBox) view.findViewById(R.id.componente29_P);
        componente30 = (CheckBox) view.findViewById(R.id.componente30_P);
        componente31 = (CheckBox) view.findViewById(R.id.componente31_P);
        componente32 = (CheckBox) view.findViewById(R.id.componente32_P);
        componente33 = (CheckBox) view.findViewById(R.id.componente33_P);
        componente34 = (CheckBox) view.findViewById(R.id.componente34_P);
        componente35 = (CheckBox) view.findViewById(R.id.componente35_P);
        componente36 = (CheckBox) view.findViewById(R.id.componente36_P);
        componente37 = (CheckBox) view.findViewById(R.id.componente37_P);
        componente38 = (CheckBox) view.findViewById(R.id.componente38_P);
        componente39 = (CheckBox) view.findViewById(R.id.componente39_P);
        componente40 = (CheckBox) view.findViewById(R.id.componente40_P);
    }

    private void setarSP() {
        spc1P = getActivity().getSharedPreferences(comp1P, 0);
        spc2P = getActivity().getSharedPreferences(comp2P, 0);
        spc3P = getActivity().getSharedPreferences(comp3P, 0);
        spc4P = getActivity().getSharedPreferences(comp4P, 0);
        spc5P = getActivity().getSharedPreferences(comp5P, 0);
        spc6P = getActivity().getSharedPreferences(comp6P, 0);
        spc7P = getActivity().getSharedPreferences(comp7P, 0);
        spc8P = getActivity().getSharedPreferences(comp8P, 0);
        spc9P = getActivity().getSharedPreferences(comp9P, 0);
        spc10P = getActivity().getSharedPreferences(comp10P, 0);
        spc11P = getActivity().getSharedPreferences(comp11P, 0);
        spc12P = getActivity().getSharedPreferences(comp12P, 0);
        spc13P = getActivity().getSharedPreferences(comp13P, 0);
        spc14P = getActivity().getSharedPreferences(comp14P, 0);
        spc15P = getActivity().getSharedPreferences(comp15P, 0);
        spc16P = getActivity().getSharedPreferences(comp16P, 0);
        spc17P = getActivity().getSharedPreferences(comp17P, 0);
        spc18P = getActivity().getSharedPreferences(comp18P, 0);
        spc19P = getActivity().getSharedPreferences(comp19P, 0);
        spc20P = getActivity().getSharedPreferences(comp20P, 0);
        spc21P = getActivity().getSharedPreferences(comp21P, 0);
        spc22P = getActivity().getSharedPreferences(comp22P, 0);
        spc23P = getActivity().getSharedPreferences(comp23P, 0);
        spc24P = getActivity().getSharedPreferences(comp24P, 0);
        spc25P = getActivity().getSharedPreferences(comp25P, 0);
        spc26P = getActivity().getSharedPreferences(comp26P, 0);
        spc27P = getActivity().getSharedPreferences(comp27P, 0);
        spc28P = getActivity().getSharedPreferences(comp28P, 0);
        spc29P = getActivity().getSharedPreferences(comp29P, 0);
        spc30P = getActivity().getSharedPreferences(comp30P, 0);
        spc31P = getActivity().getSharedPreferences(comp31P, 0);
        spc32P = getActivity().getSharedPreferences(comp32P, 0);
        spc33P = getActivity().getSharedPreferences(comp33P, 0);
        spc34P = getActivity().getSharedPreferences(comp34P, 0);
        spc35P = getActivity().getSharedPreferences(comp35P, 0);
        spc36P = getActivity().getSharedPreferences(comp36P, 0);
        spc37P = getActivity().getSharedPreferences(comp37P, 0);
        spc38P = getActivity().getSharedPreferences(comp38P, 0);
        spc39P = getActivity().getSharedPreferences(comp39P, 0);
        spc40P = getActivity().getSharedPreferences(comp40P, 0);

        ec1P = spc1P.edit();
        ec2P = spc2P.edit();
        ec3P = spc3P.edit();
        ec4P = spc4P.edit();
        ec5P = spc5P.edit();
        ec6P = spc6P.edit();
        ec7P = spc7P.edit();
        ec8P = spc8P.edit();
        ec9P = spc9P.edit();
        ec10P = spc10P.edit();
        ec11P = spc11P.edit();
        ec12P = spc12P.edit();
        ec13P = spc13P.edit();
        ec14P = spc14P.edit();
        ec15P = spc15P.edit();
        ec16P = spc16P.edit();
        ec17P = spc17P.edit();
        ec18P = spc18P.edit();
        ec19P = spc19P.edit();
        ec20P = spc20P.edit();
        ec21P = spc21P.edit();
        ec22P = spc22P.edit();
        ec23P = spc23P.edit();
        ec24P = spc24P.edit();
        ec25P = spc25P.edit();
        ec26P = spc26P.edit();
        ec27P = spc27P.edit();
        ec28P = spc28P.edit();
        ec29P = spc29P.edit();
        ec30P = spc30P.edit();
        ec31P = spc31P.edit();
        ec32P = spc32P.edit();
        ec33P = spc33P.edit();
        ec34P = spc34P.edit();
        ec35P = spc35P.edit();
        ec36P = spc36P.edit();
        ec37P = spc37P.edit();
        ec38P = spc38P.edit();
        ec39P = spc39P.edit();
        ec40P = spc40P.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);
        componente5.setOnClickListener(this);
        componente6.setOnClickListener(this);
        componente7.setOnClickListener(this);
        componente8.setOnClickListener(this);
        componente9.setOnClickListener(this);
        componente10.setOnClickListener(this);
        componente11.setOnClickListener(this);
        componente12.setOnClickListener(this);
        componente13.setOnClickListener(this);
        componente14.setOnClickListener(this);
        componente15.setOnClickListener(this);
        componente16.setOnClickListener(this);
        componente17.setOnClickListener(this);
        componente18.setOnClickListener(this);
        componente19.setOnClickListener(this);
        componente20.setOnClickListener(this);
        componente21.setOnClickListener(this);
        componente22.setOnClickListener(this);
        componente23.setOnClickListener(this);
        componente24.setOnClickListener(this);
        componente25.setOnClickListener(this);
        componente26.setOnClickListener(this);
        componente27.setOnClickListener(this);
        componente28.setOnClickListener(this);
        componente29.setOnClickListener(this);
        componente30.setOnClickListener(this);
        componente31.setOnClickListener(this);
        componente32.setOnClickListener(this);
        componente33.setOnClickListener(this);
        componente34.setOnClickListener(this);
        componente35.setOnClickListener(this);
        componente36.setOnClickListener(this);
        componente37.setOnClickListener(this);
        componente38.setOnClickListener(this);
        componente39.setOnClickListener(this);
        componente40.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_P:
                ec1P.putBoolean("estado1P", componente1.isChecked());
                ec1P.commit();
                break;
            case R.id.componente2_P:
                ec2P.putBoolean("estado2P", componente2.isChecked());
                ec2P.commit();
                break;
            case R.id.componente3_P:
                ec3P.putBoolean("estado3P", componente3.isChecked());
                ec3P.commit();
                break;
            case R.id.componente4_P:
                ec4P.putBoolean("estado4P", componente4.isChecked());
                ec4P.commit();
                break;
            case R.id.componente5_P:
                ec5P.putBoolean("estado5P", componente5.isChecked());
                ec5P.commit();
                break;
            case R.id.componente6_P:
                ec6P.putBoolean("estado6P", componente6.isChecked());
                ec6P.commit();
                break;
            case R.id.componente7_P:
                ec7P.putBoolean("estado7P", componente7.isChecked());
                ec7P.commit();
                break;
            case R.id.componente8_P:
                ec8P.putBoolean("estado8P", componente8.isChecked());
                ec8P.commit();
                break;
            case R.id.componente9_P:
                ec9P.putBoolean("estado9P", componente9.isChecked());
                ec9P.commit();
                break;
            case R.id.componente10_P:
                ec10P.putBoolean("estado10P", componente10.isChecked());
                ec10P.commit();
                break;
            case R.id.componente11_P:
                ec11P.putBoolean("estado11P", componente11.isChecked());
                ec11P.commit();
                break;
            case R.id.componente12_P:
                ec12P.putBoolean("estado12P", componente12.isChecked());
                ec12P.commit();
                break;
            case R.id.componente13_P:
                ec13P.putBoolean("estado13P", componente13.isChecked());
                ec13P.commit();
                break;
            case R.id.componente14_P:
                ec14P.putBoolean("estado14P", componente14.isChecked());
                ec14P.commit();
                break;
            case R.id.componente15_P:
                ec15P.putBoolean("estado15P", componente15.isChecked());
                ec15P.commit();
                break;
            case R.id.componente16_P:
                ec16P.putBoolean("estado16P", componente16.isChecked());
                ec16P.commit();
                break;
            case R.id.componente17_P:
                ec17P.putBoolean("estado17P", componente17.isChecked());
                ec17P.commit();
                break;
            case R.id.componente18_P:
                ec18P.putBoolean("estado18P", componente18.isChecked());
                ec18P.commit();
                break;
            case R.id.componente19_P:
                ec19P.putBoolean("estado19P", componente19.isChecked());
                ec19P.commit();
                break;
            case R.id.componente20_P:
                ec20P.putBoolean("estado20P", componente20.isChecked());
                ec20P.commit();
                break;
            case R.id.componente21_P:
                ec21P.putBoolean("estado21P", componente21.isChecked());
                ec21P.commit();
                break;
            case R.id.componente22_P:
                ec22P.putBoolean("estado22P", componente22.isChecked());
                ec22P.commit();
                break;
            case R.id.componente23_P:
                ec23P.putBoolean("estado23P", componente23.isChecked());
                ec23P.commit();
                break;
            case R.id.componente24_P:
                ec24P.putBoolean("estado24P", componente24.isChecked());
                ec24P.commit();
                break;
            case R.id.componente25_P:
                ec25P.putBoolean("estado25P", componente25.isChecked());
                ec25P.commit();
                break;
            case R.id.componente26_P:
                ec26P.putBoolean("estado26P", componente26.isChecked());
                ec26P.commit();
                break;
            case R.id.componente27_P:
                ec27P.putBoolean("estado27P", componente27.isChecked());
                ec27P.commit();
                break;
            case R.id.componente28_P:
                ec28P.putBoolean("estado28P", componente28.isChecked());
                ec28P.commit();
                break;
            case R.id.componente29_P:
                ec29P.putBoolean("estado29P", componente29.isChecked());
                ec29P.commit();
                break;
            case R.id.componente30_P:
                ec30P.putBoolean("estado30P", componente30.isChecked());
                ec30P.commit();
                break;
            case R.id.componente31_P:
                ec31P.putBoolean("estado31P", componente31.isChecked());
                ec31P.commit();
                break;
            case R.id.componente32_P:
                ec32P.putBoolean("estado32P", componente32.isChecked());
                ec32P.commit();
                break;
            case R.id.componente33_P:
                ec33P.putBoolean("estado33P", componente33.isChecked());
                ec33P.commit();
                break;
            case R.id.componente34_P:
                ec34P.putBoolean("estado34P", componente34.isChecked());
                ec34P.commit();
                break;
            case R.id.componente35_P:
                ec35P.putBoolean("estado35P", componente35.isChecked());
                ec35P.commit();
                break;
            case R.id.componente36_P:
                ec36P.putBoolean("estado36P", componente36.isChecked());
                ec36P.commit();
                break;
            case R.id.componente37_P:
                ec37P.putBoolean("estado37P", componente37.isChecked());
                ec37P.commit();
                break;
            case R.id.componente38_P:
                ec38P.putBoolean("estado38P", componente38.isChecked());
                ec38P.commit();
                break;
            case R.id.componente39_P:
                ec39P.putBoolean("estado39P", componente39.isChecked());
                ec39P.commit();
                break;
            case R.id.componente40_P:
                ec40P.putBoolean("estado40P", componente40.isChecked());
                ec40P.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1P.putBoolean("estado1P", false);
            ec1P.commit();

            ec2P.putBoolean("estado2P", false);
            ec2P.commit();

            ec3P.putBoolean("estado3P", false);
            ec3P.commit();

            ec4P.putBoolean("estado4P", false);
            ec4P.commit();

            ec5P.putBoolean("estado5P", false);
            ec5P.commit();

            ec6P.putBoolean("estado6P", false);
            ec6P.commit();

            ec7P.putBoolean("estado7P", false);
            ec7P.commit();

            ec8P.putBoolean("estado8P", false);
            ec8P.commit();

            ec9P.putBoolean("estado9P", false);
            ec9P.commit();

            ec10P.putBoolean("estado10P", false);
            ec10P.commit();

            ec11P.putBoolean("estado11P", false);
            ec11P.commit();

            ec12P.putBoolean("estado12P", false);
            ec12P.commit();

            ec13P.putBoolean("estado13P", false);
            ec13P.commit();

            ec14P.putBoolean("estado14P", false);
            ec14P.commit();

            ec15P.putBoolean("estado15P", false);
            ec15P.commit();

            ec16P.putBoolean("estado16P", false);
            ec16P.commit();

            ec17P.putBoolean("estado17P", false);
            ec17P.commit();

            ec18P.putBoolean("estado18P", false);
            ec18P.commit();

            ec19P.putBoolean("estado19P", false);
            ec19P.commit();

            ec20P.putBoolean("estado20P", false);
            ec20P.commit();

            ec21P.putBoolean("estado21P", false);
            ec21P.commit();

            ec22P.putBoolean("estado22P", false);
            ec22P.commit();

            ec23P.putBoolean("estado23P", false);
            ec23P.commit();

            ec24P.putBoolean("estado24P", false);
            ec24P.commit();

            ec25P.putBoolean("estado25P", false);
            ec25P.commit();

            ec26P.putBoolean("estado26P", false);
            ec26P.commit();

            ec27P.putBoolean("estado27P", false);
            ec27P.commit();

            ec28P.putBoolean("estado28P", false);
            ec28P.commit();

            ec29P.putBoolean("estado29P", false);
            ec29P.commit();

            ec30P.putBoolean("estado30P", false);
            ec30P.commit();

            ec31P.putBoolean("estado31P", false);
            ec31P.commit();

            ec32P.putBoolean("estado32P", false);
            ec32P.commit();

            ec33P.putBoolean("estado33P", false);
            ec33P.commit();

            ec34P.putBoolean("estado34P", false);
            ec34P.commit();

            ec35P.putBoolean("estado35P", false);
            ec35P.commit();

            ec36P.putBoolean("estado36P", false);
            ec36P.commit();

            ec37P.putBoolean("estado37P", false);
            ec37P.commit();

            ec38P.putBoolean("estado38P", false);
            ec38P.commit();

            ec39P.putBoolean("estado39P", false);
            ec39P.commit();

            ec40P.putBoolean("estado40P", false);
            ec40P.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}