package com.gustavosg.cabelossaudaveis.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDCore extends SQLiteOpenHelper {
    private static final String NOME_DB = "cabelossaudaveisbd";
    private static final int VERSAO_BD = 1;

    public BDCore(Context context) {
        super(context, NOME_DB, null, VERSAO_BD);
    }


    @Override
    public void onCreate(SQLiteDatabase bd) {

        bd.execSQL("create table cronograma(clique char(4) primary key, categoria varchar(12) not null, " +
                "dia date not null, cor int);");
    }


    @Override
    public void onUpgrade(SQLiteDatabase bd, int oldVersion, int newVersion) {
        onCreate(bd);
    }
}

