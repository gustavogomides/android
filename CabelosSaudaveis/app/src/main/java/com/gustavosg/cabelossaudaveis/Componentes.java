package com.gustavosg.cabelossaudaveis;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gustavosg.cabelossaudaveis.tab.AFragment;
import com.gustavosg.cabelossaudaveis.tab.BFragment;
import com.gustavosg.cabelossaudaveis.tab.CFragment;
import com.gustavosg.cabelossaudaveis.tab.DFragment;
import com.gustavosg.cabelossaudaveis.tab.EFragment;
import com.gustavosg.cabelossaudaveis.tab.HFragment;
import com.gustavosg.cabelossaudaveis.tab.IFragment;
import com.gustavosg.cabelossaudaveis.tab.LFragment;
import com.gustavosg.cabelossaudaveis.tab.MFragment;
import com.gustavosg.cabelossaudaveis.tab.PFragment;
import com.gustavosg.cabelossaudaveis.tab.SFragment;
import com.gustavosg.cabelossaudaveis.tab.TFragment;
import com.gustavosg.cabelossaudaveis.tab.VFragment;

import java.util.ArrayList;
import java.util.List;

public class Componentes extends AppCompatActivity {

    // letra a
    public boolean comp1A;
    public boolean comp2A;
    public boolean comp3A;
    public boolean comp4A;
    public boolean comp5A;
    public boolean comp6A;
    public boolean comp7A;
    public boolean comp8A;
    public boolean comp9A;

    // letra b
    public boolean comp1B;
    public boolean comp2B;

    // letra c
    public boolean comp1C;
    public boolean comp2C;
    public boolean comp3C;
    public boolean comp4C;
    public boolean comp5C;
    public boolean comp6C;
    public boolean comp7C;
    public boolean comp8C;
    public boolean comp9C;

    // letra d
    public boolean comp1D;
    public boolean comp2D;
    public boolean comp3D;
    public boolean comp4D;
    public boolean comp5D;
    public boolean comp6D;
    public boolean comp7D;
    public boolean comp8D;
    public boolean comp9D;
    public boolean comp10D;
    public boolean comp11D;
    public boolean comp12D;
    public boolean comp13D;
    public boolean comp14D;
    public boolean comp15D;
    public boolean comp16D;
    public boolean comp17D;
    public boolean comp18D;
    public boolean comp19D;
    public boolean comp20D;
    public boolean comp21D;

    // letra e
    public boolean comp1E;
    public boolean comp2E;

    // letra h
    public boolean comp1H;
    public boolean comp2H;

    // letra i
    public boolean comp1I;
    public boolean comp2I;
    public boolean comp3I;

    // letra L
    public boolean comp1L;
    public boolean comp2L;
    public boolean comp3L;

    // letra M
    public boolean comp1M;
    public boolean comp2M;
    public boolean comp3M;
    public boolean comp4M;
    public boolean comp5M;
    public boolean comp6M;

    // letra P
    public boolean comp1P;
    public boolean comp2P;
    public boolean comp3P;
    public boolean comp4P;
    public boolean comp5P;
    public boolean comp6P;
    public boolean comp7P;
    public boolean comp8P;
    public boolean comp9P;
    public boolean comp10P;
    public boolean comp11P;
    public boolean comp12P;
    public boolean comp13P;
    public boolean comp14P;
    public boolean comp15P;
    public boolean comp16P;
    public boolean comp17P;
    public boolean comp18P;
    public boolean comp19P;
    public boolean comp20P;
    public boolean comp21P;
    public boolean comp22P;
    public boolean comp23P;
    public boolean comp24P;
    public boolean comp25P;
    public boolean comp26P;
    public boolean comp27P;
    public boolean comp28P;
    public boolean comp29P;
    public boolean comp30P;
    public boolean comp31P;
    public boolean comp32P;
    public boolean comp33P;
    public boolean comp34P;
    public boolean comp35P;
    public boolean comp36P;
    public boolean comp37P;
    public boolean comp38P;
    public boolean comp39P;
    public boolean comp40P;

    // letra S
    public boolean comp1S;
    public boolean comp2S;
    public boolean comp3S;
    public boolean comp4S;
    public boolean comp5S;
    public boolean comp6S;
    public boolean comp7S;
    public boolean comp8S;
    public boolean comp9S;
    public boolean comp10S;
    public boolean comp11S;
    public boolean comp12S;
    public boolean comp13S;
    public boolean comp14S;
    public boolean comp15S;
    public boolean comp16S;
    public boolean comp17S;
    public boolean comp18S;
    public boolean comp19S;
    public boolean comp20S;
    public boolean comp21S;
    public boolean comp22S;

    // letra T
    public boolean comp1T;
    public boolean comp2T;
    public boolean comp3T;
    public boolean comp4T;

    // letra V
    public boolean comp1V;
    public boolean comp2V;
    public boolean comp3V;

    // tab
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_componentes);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AFragment(), "A");
        adapter.addFragment(new BFragment(), "B");
        adapter.addFragment(new CFragment(), "C");
        adapter.addFragment(new DFragment(), "D");
        adapter.addFragment(new EFragment(), "E");
        adapter.addFragment(new HFragment(), "H");
        adapter.addFragment(new IFragment(), "I");
        adapter.addFragment(new LFragment(), "L");
        adapter.addFragment(new MFragment(), "M");
        adapter.addFragment(new PFragment(), "P");
        adapter.addFragment(new SFragment(), "S");
        adapter.addFragment(new TFragment(), "T");
        adapter.addFragment(new VFragment(), "V");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_componentes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.analisarComponente) {
            setBoolean();
            analisar();

        }

        return super.onOptionsItemSelected(item);
    }

    private void analisar() {

        LayoutInflater factory = LayoutInflater.from(this);
        View view = factory.inflate(
                R.layout.analisar, null);

        TextView tituloProibidoSemShampoo = (TextView) view.findViewById(R.id.tituloProibidoSemShampoo);
        TextView componentesProibidoSemShampoo = (TextView) view.findViewById(R.id.componentesProibidoSemShampoo);

        TextView tituloProibidoAmbos = (TextView) view.findViewById(R.id.tituloProibidoAmbos);
        TextView componentesProibidoAmbos = (TextView) view.findViewById(R.id.componentesProibidoAmbos);

        TextView tituloProibidoCoWash = (TextView) view.findViewById(R.id.tituloProibidoCoWash);
        TextView componentesProibidoCoWash = (TextView) view.findViewById(R.id.componentesProibidoCoWash);

        TextView tituloLiberadoPoucoShampoo = (TextView) view.findViewById(R.id.tituloLiberadoPoucoShampoo);
        TextView componentesLiberadoPoucoShampoo = (TextView) view.findViewById(R.id.componentesLiberadoPoucoShampoo);

        TextView tituloLiberadoAmbos = (TextView) view.findViewById(R.id.tituloLiberadoAmbos);
        TextView componentesLiberadoAmbos = (TextView) view.findViewById(R.id.componentesLiberadoAmbos);

        setarTV(componentesProibidoSemShampoo, componentesProibidoAmbos, componentesLiberadoPoucoShampoo, componentesLiberadoAmbos, componentesProibidoCoWash);

        if (componentesProibidoSemShampoo.getText().equals("")
                && componentesProibidoAmbos.getText().equals("")
                && componentesProibidoCoWash.getText().equals("")
                && componentesLiberadoPoucoShampoo.getText().equals("")
                && componentesLiberadoAmbos.getText().equals("")) {
            String saida = "Selecione pelo menos um componente!";
            Toast.makeText(this, saida, Toast.LENGTH_SHORT).show();
        } else {
            if (componentesProibidoSemShampoo.getText().equals("")) {
                tituloProibidoSemShampoo.setVisibility(View.GONE);
                componentesProibidoSemShampoo.setVisibility(View.GONE);
            }

            if (componentesProibidoAmbos.getText().equals("")) {
                tituloProibidoAmbos.setVisibility(View.GONE);
                componentesProibidoAmbos.setVisibility(View.GONE);
            }

            if (componentesProibidoCoWash.getText().equals("")) {
                tituloProibidoCoWash.setVisibility(View.GONE);
                componentesProibidoCoWash.setVisibility(View.GONE);
            }

            if (componentesLiberadoPoucoShampoo.getText().equals("")) {
                tituloLiberadoPoucoShampoo.setVisibility(View.GONE);
                componentesLiberadoPoucoShampoo.setVisibility(View.GONE);
            }

            if (componentesLiberadoAmbos.getText().equals("")) {
                tituloLiberadoAmbos.setVisibility(View.GONE);
                componentesLiberadoAmbos.setVisibility(View.GONE);
            }

            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setView(view);


            view.findViewById(R.id.btVoltar).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
        }

    }

    public void setarTV(TextView componentesProibidoSemShampoo, TextView componentesProibidoAmbos,
                        TextView componentesLiberadoPoucoShampoo, TextView componentesLiberadoAmbos,
                        TextView componentesProibidoCoWash) {

        componentesProibidoSemShampoo(componentesProibidoSemShampoo);

        componentesProibidoAmbos(componentesProibidoAmbos);

        componentesProibidoCoWash(componentesProibidoCoWash);

        componentesLiberadoPoucoShampoo(componentesLiberadoPoucoShampoo);

        componentesLiberadoAmbos(componentesLiberadoAmbos);
    }

    private void componentesProibidoSemShampoo(TextView componentesProibidoSemShampoo) {
        String saida = "";
        if (comp2A) {
            saida += getResources().getString(R.string.c2a) + "\n" + "\n";
        }

        if (comp1B) {
            saida += getResources().getString(R.string.c1b) + "\n" + "\n";
        }

        if (comp2B) {
            saida += getResources().getString(R.string.c2b) + "\n" + "\n";
        }


        if (comp3C) {
            saida += getResources().getString(R.string.c3c) + "\n" + "\n";
        }

        if (comp4C) {
            saida += getResources().getString(R.string.c4c) + "\n" + "\n";
        }

        if (comp5C) {
            saida += getResources().getString(R.string.c5c) + " " + getResources().getString(R.string.c5cc) + "\n" + "\n";
        }

        if (comp6C) {
            saida += getResources().getString(R.string.c6c) + " " + getResources().getString(R.string.c6cc) + "\n" + "\n";
        }

        if (comp7C) {
            saida += getResources().getString(R.string.c7c) + "\n" + "\n";
        }

        if (comp8C) {
            saida += getResources().getString(R.string.c8c) + "\n" + "\n";
        }

        if (comp9C) {
            saida += getResources().getString(R.string.c9c) + "\n" + "\n";
        }

        if (comp1D) {
            saida += getResources().getString(R.string.c1d) + "\n" + "\n";
        }

        if (comp2D) {
            saida += getResources().getString(R.string.c2d) + "\n" + "\n";
        }

        if (comp4D) {
            saida += getResources().getString(R.string.c4d) + "\n" + "\n";
        }

        if (comp15D) {
            saida += getResources().getString(R.string.c15d) + "\n" + "\n";
        }
        if (comp16D) {
            saida += getResources().getString(R.string.c16d) + "\n" + "\n";
        }
        if (comp17D) {
            saida += getResources().getString(R.string.c17d) + "\n" + "\n";
        }
        if (comp18D) {
            saida += getResources().getString(R.string.c18d) + "\n" + "\n";
        }
        if (comp19D) {
            saida += getResources().getString(R.string.c19d) + "\n" + "\n";
        }

        if (comp2E) {
            saida += getResources().getString(R.string.c2e) + "\n" + "\n";
        }


        if (comp3L) {
            saida += getResources().getString(R.string.c3l) + "\n" + "\n";
        }


        if (comp1M) {
            saida += getResources().getString(R.string.c1m) + "\n" + "\n";
        }

        if (comp2M) {
            saida += getResources().getString(R.string.c2m) + "\n" + "\n";
        }

        if (comp3M) {
            saida += getResources().getString(R.string.c3m) + " " + getResources().getString(R.string.c3mm) + "\n" + "\n";
        }

        if (comp4M) {
            saida += getResources().getString(R.string.c4m) + "\n" + "\n";
        }

        if (comp35P) {
            saida += getResources().getString(R.string.c35p) + "\n" + "\n";
        }

        if (comp36P) {
            saida += getResources().getString(R.string.c36p) + "\n" + "\n";
        }

        if (comp37P) {
            saida += getResources().getString(R.string.c37p) + "\n" + "\n";
        }

        if (comp38P) {
            saida += getResources().getString(R.string.c38p) + "\n" + "\n";
        }

        if (comp39P) {
            saida += getResources().getString(R.string.c39p) + "\n" + "\n";
        }

        if (comp1S) {
            saida += getResources().getString(R.string.c1s) + "\n" + "\n";
        }
        if (comp5S) {
            saida += getResources().getString(R.string.c5s) + "\n" + "\n";
        }
        if (comp6S) {
            saida += getResources().getString(R.string.c6s) + "\n" + "\n";
        }
        if (comp9S) {
            saida += getResources().getString(R.string.c9s) + "\n" + "\n";
        }
        if (comp11S) {
            saida += getResources().getString(R.string.c11s) + "\n" + "\n";
        }
        if (comp12S) {
            saida += getResources().getString(R.string.c12s) + "\n" + "\n";
        }
        if (comp13S) {
            saida += getResources().getString(R.string.c13s) + "\n" + "\n";
        }
        if (comp15S) {
            saida += getResources().getString(R.string.c15s) + "\n" + "\n";
        }
        if (comp20S) {
            saida += getResources().getString(R.string.c20s) + "\n" + "\n";
        }
        if (comp21S) {
            saida += getResources().getString(R.string.c21s) + "\n" + "\n";
        }
        if (comp22S) {
            saida += getResources().getString(R.string.c22s) + "\n" + "\n";
        }

        if (comp4T) {
            saida += getResources().getString(R.string.c4t) + "\n" + "\n";
        }

        if (comp2V) {
            saida += getResources().getString(R.string.c2v) + "\n" + "\n";
        }
        if (comp3V) {
            saida += getResources().getString(R.string.c3v) + "\n" + "\n";
        }

        componentesProibidoSemShampoo.setText(saida);
    }

    private void componentesProibidoAmbos(TextView componentesProibidoAmbos) {
        String saida = "";

        if (comp1A) {
            saida += getResources().getString(R.string.c1a) + "\n" + "\n";
        }
        if (comp3A) {
            saida += getResources().getString(R.string.c3a) + "\n" + "\n";
        }
        if (comp4A) {
            saida += getResources().getString(R.string.c4a) + "\n" + "\n";
        }
        if (comp5A) {
            saida += getResources().getString(R.string.c5a) + "\n" + "\n";
        }
        if (comp6A) {
            saida += getResources().getString(R.string.c6a) + "\n" + "\n";
        }
        if (comp7A) {
            saida += getResources().getString(R.string.c7a) + "\n" + "\n";
        }
        if (comp8A) {
            saida += getResources().getString(R.string.c8a) + "\n" + "\n";
        }


        if (comp1C) {
            saida += getResources().getString(R.string.c1c) + "\n" + "\n";
        }
        if (comp2C) {
            saida += getResources().getString(R.string.c2c) + "\n" + "\n";
        }


        if (comp20D) {
            saida += getResources().getString(R.string.c20d) + "\n" + "\n";
        }
        if (comp21D) {
            saida += getResources().getString(R.string.c21d) + "\n" + "\n";
        }

        if (comp1E) {
            saida += getResources().getString(R.string.c1e) + "\n" + "\n";
        }

        if (comp1H) {
            saida += getResources().getString(R.string.c1h) + "\n" + "\n";
        }


        if (comp1I) {
            saida += getResources().getString(R.string.c1i) + "\n" + "\n";
        }
        if (comp2I) {
            saida += getResources().getString(R.string.c2i) + "\n" + "\n";
        }
        if (comp3I) {
            saida += getResources().getString(R.string.c3i) + "\n" + "\n";
        }

        if (comp2L) {
            saida += getResources().getString(R.string.c2l) + "\n" + "\n";
        }

        if (comp5M) {
            saida += getResources().getString(R.string.c5m) + "\n" + "\n";
        }

        if (comp6M) {
            saida += getResources().getString(R.string.c6m) + "\n" + "\n";
        }

        if (comp1P) {
            saida += getResources().getString(R.string.c1p) + "\n" + "\n";
        }
        if (comp34P) {
            saida += getResources().getString(R.string.c34p) + "\n" + "\n";
        }
        if (comp40P) {
            saida += getResources().getString(R.string.c40p) + "\n" + "\n";
        }

        if (comp2S) {
            saida += getResources().getString(R.string.c2s) + "\n" + "\n";
        }
        if (comp3S) {
            saida += getResources().getString(R.string.c3s) + "\n" + "\n";
        }
        if (comp4S) {
            saida += getResources().getString(R.string.c4s) + "\n" + "\n";
        }
        if (comp7S) {
            saida += getResources().getString(R.string.c7s) + "\n" + "\n";
        }
        if (comp8S) {
            saida += getResources().getString(R.string.c8s) + "\n" + "\n";
        }
        if (comp10S) {
            saida += getResources().getString(R.string.c10s) + "\n" + "\n";
        }
        if (comp14S) {
            saida += getResources().getString(R.string.c14s) + "\n" + "\n";
        }
        if (comp16S) {
            saida += getResources().getString(R.string.c16s) + "\n" + "\n";
        }
        if (comp17S) {
            saida += getResources().getString(R.string.c17s) + "\n" + "\n";
        }
        if (comp18S) {
            saida += getResources().getString(R.string.c18s) + "\n" + "\n";
        }
        if (comp19S) {
            saida += getResources().getString(R.string.c19s) + "\n" + "\n";
        }

        if (comp1T) {
            saida += getResources().getString(R.string.c1t) + " " + getResources().getString(R.string.c1tt) + "\n" + "\n";
        }
        if (comp2T) {
            saida += getResources().getString(R.string.c2t) + "\n" + "\n";
        }
        if (comp3T) {
            saida += getResources().getString(R.string.c3t) + "\n" + "\n";
        }

        if (comp1V) {
            saida += getResources().getString(R.string.c1v) + "\n" + "\n";
        }
        componentesProibidoAmbos.setText(saida);
    }

    private void componentesProibidoCoWash(TextView componentesProibidoCoWash) {
        String saida = "";
        if (comp2A) {
            saida += getResources().getString(R.string.c2a) + "\n" + "\n";
        }

        if (comp9A) {
            saida += getResources().getString(R.string.c9a) + "\n" + "\n";
        }

        if (comp3C) {
            saida += getResources().getString(R.string.c3c) + "\n" + "\n";
        }

        if (comp4C) {
            saida += getResources().getString(R.string.c4c) + "\n" + "\n";
        }

        if (comp8C) {
            saida += getResources().getString(R.string.c8c) + "\n" + "\n";
        }

        if (comp9C) {
            saida += getResources().getString(R.string.c9c) + "\n" + "\n";
        }

        if (comp2D) {
            saida += getResources().getString(R.string.c2d) + "\n" + "\n";
        }

        if (comp3D) {
            saida += getResources().getString(R.string.c3d) + "\n" + "\n";
        }

        if (comp5D) {
            saida += getResources().getString(R.string.c5d) + "\n" + "\n";
        }

        if (comp6D) {
            saida += getResources().getString(R.string.c6d) + "\n" + "\n";
        }
        if (comp7D) {
            saida += getResources().getString(R.string.c7d) + "\n" + "\n";
        }
        if (comp8D) {
            saida += getResources().getString(R.string.c8d) + "\n" + "\n";
        }
        if (comp9D) {
            saida += getResources().getString(R.string.c9d) + "\n" + "\n";
        }
        if (comp10D) {
            saida += getResources().getString(R.string.c10d) + "\n" + "\n";
        }
        if (comp11D) {
            saida += getResources().getString(R.string.c11d) + "\n" + "\n";
        }
        if (comp12D) {
            saida += getResources().getString(R.string.c12d) + "\n" + "\n";
        }
        if (comp13D) {
            saida += getResources().getString(R.string.c13d) + "\n" + "\n";
        }
        if (comp14D) {
            saida += getResources().getString(R.string.c14d) + "\n" + "\n";
        }
        if (comp15D) {
            saida += getResources().getString(R.string.c15d) + "\n" + "\n";
        }
        if (comp16D) {
            saida += getResources().getString(R.string.c16d) + "\n" + "\n";
        }

        if (comp2H) {
            saida += getResources().getString(R.string.c2h) + "\n" + "\n";
        }

        if (comp1I) {
            saida += getResources().getString(R.string.c1i) + "\n" + "\n";
        }
        if (comp3I) {
            saida += getResources().getString(R.string.c3i) + "\n" + "\n";
        }

        if (comp1L) {
            saida += getResources().getString(R.string.c1l) + "\n" + "\n";
        }

        if (comp1M) {
            saida += getResources().getString(R.string.c1m) + "\n" + "\n";
        }
        if (comp5M) {
            saida += getResources().getString(R.string.c5m) + "\n" + "\n";
        }

        if (comp1P) {
            saida += getResources().getString(R.string.c1p) + "\n" + "\n";
        }
        if (comp34P) {
            saida += getResources().getString(R.string.c34p) + "\n" + "\n";
        }

        if (comp2P) {
            saida += getResources().getString(R.string.c2p) + "\n" + "\n";
        }
        if (comp3P) {
            saida += getResources().getString(R.string.c3p) + "\n" + "\n";
        }
        if (comp4P) {
            saida += getResources().getString(R.string.c4p) + "\n" + "\n";
        }
        if (comp5P) {
            saida += getResources().getString(R.string.c5p) + "\n" + "\n";
        }
        if (comp6P) {
            saida += getResources().getString(R.string.c6p) + "\n" + "\n";
        }
        if (comp7P) {
            saida += getResources().getString(R.string.c7p) + "\n" + "\n";
        }
        if (comp8P) {
            saida += getResources().getString(R.string.c8p) + "\n" + "\n";
        }
        if (comp9P) {
            saida += getResources().getString(R.string.c9p) + "\n" + "\n";
        }
        if (comp10P) {
            saida += getResources().getString(R.string.c10p) + "\n" + "\n";
        }
        if (comp11P) {
            saida += getResources().getString(R.string.c11p) + "\n" + "\n";
        }
        if (comp12P) {
            saida += getResources().getString(R.string.c12p) + "\n" + "\n";
        }
        if (comp13P) {
            saida += getResources().getString(R.string.c13p) + "\n" + "\n";
        }
        if (comp14P) {
            saida += getResources().getString(R.string.c14p) + "\n" + "\n";
        }
        if (comp15P) {
            saida += getResources().getString(R.string.c15p) + "\n" + "\n";
        }
        if (comp16P) {
            saida += getResources().getString(R.string.c16p) + "\n" + "\n";
        }
        if (comp17P) {
            saida += getResources().getString(R.string.c17p) + "\n" + "\n";
        }
        if (comp18P) {
            saida += getResources().getString(R.string.c18p) + "\n" + "\n";
        }
        if (comp19P) {
            saida += getResources().getString(R.string.c19p) + "\n" + "\n";
        }
        if (comp20P) {
            saida += getResources().getString(R.string.c20p) + "\n" + "\n";
        }
        if (comp21P) {
            saida += getResources().getString(R.string.c21p) + "\n" + "\n";
        }
        if (comp22P) {
            saida += getResources().getString(R.string.c22p) + " " + getResources().getString(R.string.c22pp) + "\n" + "\n";
        }
        if (comp23P) {
            saida += getResources().getString(R.string.c23p) + "\n" + "\n";
        }
        if (comp24P) {
            saida += getResources().getString(R.string.c24p) + "\n" + "\n";
        }
        if (comp25P) {
            saida += getResources().getString(R.string.c25p) + "\n" + "\n";
        }
        if (comp26P) {
            saida += getResources().getString(R.string.c26p) + "\n" + "\n";
        }
        if (comp27P) {
            saida += getResources().getString(R.string.c27p) + "\n" + "\n";
        }
        if (comp28P) {
            saida += getResources().getString(R.string.c28p) + " " + getResources().getString(R.string.c28pp) + "\n" + "\n";
        }
        if (comp29P) {
            saida += getResources().getString(R.string.c29p) + "\n" + "\n";
        }
        if (comp30P) {
            saida += getResources().getString(R.string.c30p) + "\n" + "\n";
        }
        if (comp31P) {
            saida += getResources().getString(R.string.c31p) + "\n" + "\n";
        }
        if (comp32P) {
            saida += getResources().getString(R.string.c32p) + "\n" + "\n";
        }
        if (comp33P) {
            saida += getResources().getString(R.string.c33p) + "\n" + "\n";
        }
        if (comp35P) {
            saida += getResources().getString(R.string.c35p) + "\n" + "\n";
        }
        if (comp38P) {
            saida += getResources().getString(R.string.c38p) + "\n" + "\n";
        }
        if (comp21S) {
            saida += getResources().getString(R.string.c21s) + "\n" + "\n";
        }


        if (comp4T) {
            saida += getResources().getString(R.string.c4t) + "\n" + "\n";
        }

        if (comp1V) {
            saida += getResources().getString(R.string.c1v) + "\n" + "\n";
        }
        componentesProibidoCoWash.setText(saida);
    }

    private void componentesLiberadoPoucoShampoo(TextView componentesLiberadoPoucoShampoo) {
        String saida = "";
        if (comp2A) {
            saida += getResources().getString(R.string.c2a) + "\n" + "\n";
        }

        if (comp1B) {
            saida += getResources().getString(R.string.c1b) + "\n" + "\n";
        }

        if (comp2B) {
            saida += getResources().getString(R.string.c2b) + "\n" + "\n";
        }


        if (comp3C) {
            saida += getResources().getString(R.string.c3c) + "\n" + "\n";
        }

        if (comp4C) {
            saida += getResources().getString(R.string.c4c) + "\n" + "\n";
        }

        if (comp5C) {
            saida += getResources().getString(R.string.c5c) + " " + getResources().getString(R.string.c5cc) + "\n" + "\n";
        }

        if (comp6C) {
            saida += getResources().getString(R.string.c6c) + " " + getResources().getString(R.string.c6cc) + "\n" + "\n";
        }

        if (comp7C) {
            saida += getResources().getString(R.string.c7c) + "\n" + "\n";
        }

        if (comp8C) {
            saida += getResources().getString(R.string.c8c) + "\n" + "\n";
        }

        if (comp9C) {
            saida += getResources().getString(R.string.c9c) + "\n" + "\n";
        }

        if (comp1D) {
            saida += getResources().getString(R.string.c1d) + "\n" + "\n";
        }

        if (comp2D) {
            saida += getResources().getString(R.string.c2d) + "\n" + "\n";
        }

        if (comp4D) {
            saida += getResources().getString(R.string.c4d) + "\n" + "\n";
        }

        if (comp15D) {
            saida += getResources().getString(R.string.c15d) + "\n" + "\n";
        }
        if (comp16D) {
            saida += getResources().getString(R.string.c16d) + "\n" + "\n";
        }
        if (comp17D) {
            saida += getResources().getString(R.string.c17d) + "\n" + "\n";
        }
        if (comp18D) {
            saida += getResources().getString(R.string.c18d) + "\n" + "\n";
        }
        if (comp19D) {
            saida += getResources().getString(R.string.c19d) + "\n" + "\n";
        }

        if (comp2E) {
            saida += getResources().getString(R.string.c2e) + "\n" + "\n";
        }

        if (comp3L) {
            saida += getResources().getString(R.string.c3l) + "\n" + "\n";
        }

        if (comp3L) {
            saida += getResources().getString(R.string.c3l) + "\n" + "\n";
        }


        if (comp1M) {
            saida += getResources().getString(R.string.c1m) + "\n" + "\n";
        }

        if (comp2M) {
            saida += getResources().getString(R.string.c2m) + "\n" + "\n";
        }


        if (comp3M) {
            saida += getResources().getString(R.string.c3m) + " " + getResources().getString(R.string.c3mm) + "\n" + "\n";
        }

        if (comp4M) {
            saida += getResources().getString(R.string.c4m) + "\n" + "\n";
        }

        if (comp35P) {
            saida += getResources().getString(R.string.c35p) + "\n" + "\n";
        }

        if (comp36P) {
            saida += getResources().getString(R.string.c36p) + "\n" + "\n";
        }

        if (comp37P) {
            saida += getResources().getString(R.string.c37p) + "\n" + "\n";
        }

        if (comp38P) {
            saida += getResources().getString(R.string.c38p) + "\n" + "\n";
        }

        if (comp39P) {
            saida += getResources().getString(R.string.c39p) + "\n" + "\n";
        }

        if (comp1S) {
            saida += getResources().getString(R.string.c1s) + "\n" + "\n";
        }
        if (comp5S) {
            saida += getResources().getString(R.string.c5s) + "\n" + "\n";
        }
        if (comp6S) {
            saida += getResources().getString(R.string.c6s) + "\n" + "\n";
        }
        if (comp9S) {
            saida += getResources().getString(R.string.c9s) + "\n" + "\n";
        }
        if (comp11S) {
            saida += getResources().getString(R.string.c11s) + "\n" + "\n";
        }
        if (comp12S) {
            saida += getResources().getString(R.string.c12s) + "\n" + "\n";
        }
        if (comp13S) {
            saida += getResources().getString(R.string.c13s) + "\n" + "\n";
        }
        if (comp15S) {
            saida += getResources().getString(R.string.c15s) + "\n" + "\n";
        }
        if (comp20S) {
            saida += getResources().getString(R.string.c20s) + "\n" + "\n";
        }
        if (comp21S) {
            saida += getResources().getString(R.string.c21s) + "\n" + "\n";
        }
        if (comp22S) {
            saida += getResources().getString(R.string.c22s) + "\n" + "\n";
        }

        if (comp4T) {
            saida += getResources().getString(R.string.c4t) + "\n" + "\n";
        }

        if (comp2V) {
            saida += getResources().getString(R.string.c2v) + "\n" + "\n";
        }
        if (comp3V) {
            saida += getResources().getString(R.string.c3v) + "\n" + "\n";
        }

        componentesLiberadoPoucoShampoo.setText(saida);
    }

    private void componentesLiberadoAmbos(TextView componentesLiberadoAmbos) {
        String saida = "";
        if (comp9A) {
            saida += getResources().getString(R.string.c9a) + "\n" + "\n";
        }

        if (comp3D) {
            saida += getResources().getString(R.string.c3d) + "\n" + "\n";
        }

        if (comp5D) {
            saida += getResources().getString(R.string.c5d) + "\n" + "\n";
        }

        if (comp6D) {
            saida += getResources().getString(R.string.c6d) + "\n" + "\n";
        }
        if (comp7D) {
            saida += getResources().getString(R.string.c7d) + "\n" + "\n";
        }
        if (comp8D) {
            saida += getResources().getString(R.string.c8d) + "\n" + "\n";
        }
        if (comp9D) {
            saida += getResources().getString(R.string.c9d) + "\n" + "\n";
        }
        if (comp10D) {
            saida += getResources().getString(R.string.c10d) + "\n" + "\n";
        }
        if (comp11D) {
            saida += getResources().getString(R.string.c11d) + "\n" + "\n";
        }
        if (comp12D) {
            saida += getResources().getString(R.string.c12d) + "\n" + "\n";
        }
        if (comp13D) {
            saida += getResources().getString(R.string.c13d) + "\n" + "\n";
        }
        if (comp14D) {
            saida += getResources().getString(R.string.c14d) + "\n" + "\n";
        }

        if (comp2H) {
            saida += getResources().getString(R.string.c2h) + "\n" + "\n";
        }


        if (comp1L) {
            saida += getResources().getString(R.string.c1l) + "\n" + "\n";
        }

        if (comp2P) {
            saida += getResources().getString(R.string.c2p) + "\n" + "\n";
        }
        if (comp3P) {
            saida += getResources().getString(R.string.c3p) + "\n" + "\n";
        }
        if (comp4P) {
            saida += getResources().getString(R.string.c4p) + "\n" + "\n";
        }
        if (comp5P) {
            saida += getResources().getString(R.string.c5p) + "\n" + "\n";
        }
        if (comp6P) {
            saida += getResources().getString(R.string.c6p) + "\n" + "\n";
        }
        if (comp7P) {
            saida += getResources().getString(R.string.c7p) + "\n" + "\n";
        }
        if (comp8P) {
            saida += getResources().getString(R.string.c8p) + "\n" + "\n";
        }
        if (comp9P) {
            saida += getResources().getString(R.string.c9p) + "\n" + "\n";
        }
        if (comp10P) {
            saida += getResources().getString(R.string.c10p) + "\n" + "\n";
        }
        if (comp11P) {
            saida += getResources().getString(R.string.c11p) + "\n" + "\n";
        }
        if (comp12P) {
            saida += getResources().getString(R.string.c12p) + "\n" + "\n";
        }
        if (comp13P) {
            saida += getResources().getString(R.string.c13p) + "\n" + "\n";
        }
        if (comp14P) {
            saida += getResources().getString(R.string.c14p) + "\n" + "\n";
        }
        if (comp15P) {
            saida += getResources().getString(R.string.c15p) + "\n" + "\n";
        }
        if (comp16P) {
            saida += getResources().getString(R.string.c16p) + "\n" + "\n";
        }
        if (comp17P) {
            saida += getResources().getString(R.string.c17p) + "\n" + "\n";
        }
        if (comp18P) {
            saida += getResources().getString(R.string.c18p) + "\n" + "\n";
        }
        if (comp19P) {
            saida += getResources().getString(R.string.c19p) + "\n" + "\n";
        }
        if (comp20P) {
            saida += getResources().getString(R.string.c20p) + "\n" + "\n";
        }
        if (comp21P) {
            saida += getResources().getString(R.string.c21p) + "\n" + "\n";
        }
        if (comp22P) {
            saida += getResources().getString(R.string.c22p) + " " + getResources().getString(R.string.c22pp) + "\n" + "\n";
        }
        if (comp23P) {
            saida += getResources().getString(R.string.c23p) + "\n" + "\n";
        }
        if (comp24P) {
            saida += getResources().getString(R.string.c24p) + "\n" + "\n";
        }
        if (comp25P) {
            saida += getResources().getString(R.string.c25p) + "\n" + "\n";
        }
        if (comp26P) {
            saida += getResources().getString(R.string.c26p) + "\n" + "\n";
        }
        if (comp27P) {
            saida += getResources().getString(R.string.c27p) + "\n" + "\n";
        }
        if (comp28P) {
            saida += getResources().getString(R.string.c28p) + " " + getResources().getString(R.string.c28pp) + "\n" + "\n";
        }
        if (comp29P) {
            saida += getResources().getString(R.string.c29p) + "\n" + "\n";
        }
        if (comp30P) {
            saida += getResources().getString(R.string.c30p) + "\n" + "\n";
        }
        if (comp31P) {
            saida += getResources().getString(R.string.c31p) + "\n" + "\n";
        }
        if (comp32P) {
            saida += getResources().getString(R.string.c32p) + "\n" + "\n";
        }
        if (comp33P) {
            saida += getResources().getString(R.string.c33p) + "\n" + "\n";
        }

        componentesLiberadoAmbos.setText(saida);
    }

    private void setBoolean() {
        // letra A
        letraA();

        // letra B
        letraB();

        // letra C
        letraC();

        // letra D
        letraD();

        // letra E
        letraE();

        // letra H
        letraH();

        // letra I
        letraI();

        // letra L
        letraL();

        // letra M
        letraM();

        // letra P
        letraP();

        // letra S
        letraS();

        // letra T
        letraT();

        // letra V
        letraV();
    }

    private void letraA() {
        SharedPreferences spcA;
        spcA = getSharedPreferences(AFragment.comp1A, 0);
        comp1A = spcA.getBoolean("estado1A", false);

        spcA = getSharedPreferences(AFragment.comp2A, 0);
        comp2A = spcA.getBoolean("estado2A", false);

        spcA = getSharedPreferences(AFragment.comp3A, 0);
        comp3A = spcA.getBoolean("estado3A", false);

        spcA = getSharedPreferences(AFragment.comp4A, 0);
        comp4A = spcA.getBoolean("estado4A", false);

        spcA = getSharedPreferences(AFragment.comp5A, 0);
        comp5A = spcA.getBoolean("estado5A", false);

        spcA = getSharedPreferences(AFragment.comp6A, 0);
        comp6A = spcA.getBoolean("estado6A", false);

        spcA = getSharedPreferences(AFragment.comp7A, 0);
        comp7A = spcA.getBoolean("estado7A", false);

        spcA = getSharedPreferences(AFragment.comp8A, 0);
        comp8A = spcA.getBoolean("estado8A", false);

        spcA = getSharedPreferences(AFragment.comp9A, 0);
        comp9A = spcA.getBoolean("estado9A", false);
    }

    private void letraB() {
        SharedPreferences spcB;
        spcB = getSharedPreferences(BFragment.comp1B, 0);
        comp1B = spcB.getBoolean("estado1B", false);

        spcB = getSharedPreferences(BFragment.comp2B, 0);
        comp2B = spcB.getBoolean("estado2B", false);
    }

    private void letraC() {
        SharedPreferences spcC;
        spcC = getSharedPreferences(CFragment.comp1C, 0);
        comp1C = spcC.getBoolean("estado1C", false);

        spcC = getSharedPreferences(CFragment.comp2C, 0);
        comp2C = spcC.getBoolean("estado2C", false);

        spcC = getSharedPreferences(CFragment.comp3C, 0);
        comp3C = spcC.getBoolean("estado3C", false);

        spcC = getSharedPreferences(CFragment.comp4C, 0);
        comp4C = spcC.getBoolean("estado4C", false);

        spcC = getSharedPreferences(CFragment.comp5C, 0);
        comp5C = spcC.getBoolean("estado5C", false);

        spcC = getSharedPreferences(CFragment.comp6C, 0);
        comp6C = spcC.getBoolean("estado6C", false);

        spcC = getSharedPreferences(CFragment.comp7C, 0);
        comp7C = spcC.getBoolean("estado7C", false);

        spcC = getSharedPreferences(CFragment.comp8C, 0);
        comp8C = spcC.getBoolean("estado8C", false);

        spcC = getSharedPreferences(CFragment.comp9C, 0);
        comp9C = spcC.getBoolean("estado9C", false);
    }

    private void letraD() {
        SharedPreferences spcD;
        spcD = getSharedPreferences(DFragment.comp1D, 0);
        comp1D = spcD.getBoolean("estado1DD", false);

        spcD = getSharedPreferences(DFragment.comp2D, 0);
        comp2D = spcD.getBoolean("estado2DD", false);

        spcD = getSharedPreferences(DFragment.comp3D, 0);
        comp3D = spcD.getBoolean("estado3DD", false);

        spcD = getSharedPreferences(DFragment.comp4D, 0);
        comp4D = spcD.getBoolean("estado4DD", false);

        spcD = getSharedPreferences(DFragment.comp5D, 0);
        comp5D = spcD.getBoolean("estado5DD", false);

        spcD = getSharedPreferences(DFragment.comp6D, 0);
        comp6D = spcD.getBoolean("estado6DD", false);

        spcD = getSharedPreferences(DFragment.comp7D, 0);
        comp7D = spcD.getBoolean("estado7DD", false);

        spcD = getSharedPreferences(DFragment.comp8D, 0);
        comp8D = spcD.getBoolean("estado8DD", false);

        spcD = getSharedPreferences(DFragment.comp9D, 0);
        comp9D = spcD.getBoolean("estado9DD", false);

        spcD = getSharedPreferences(DFragment.comp10D, 0);
        comp10D = spcD.getBoolean("estado10DD", false);

        spcD = getSharedPreferences(DFragment.comp11D, 0);
        comp11D = spcD.getBoolean("estado11DD", false);

        spcD = getSharedPreferences(DFragment.comp12D, 0);
        comp12D = spcD.getBoolean("estado12DD", false);

        spcD = getSharedPreferences(DFragment.comp13D, 0);
        comp13D = spcD.getBoolean("estado13DD", false);

        spcD = getSharedPreferences(DFragment.comp14D, 0);
        comp14D = spcD.getBoolean("estado14DD", false);

        spcD = getSharedPreferences(DFragment.comp15D, 0);
        comp15D = spcD.getBoolean("estado15DD", false);

        spcD = getSharedPreferences(DFragment.comp16D, 0);
        comp16D = spcD.getBoolean("estado16DD", false);

        spcD = getSharedPreferences(DFragment.comp17D, 0);
        comp17D = spcD.getBoolean("estado17DD", false);

        spcD = getSharedPreferences(DFragment.comp18D, 0);
        comp18D = spcD.getBoolean("estado18DD", false);

        spcD = getSharedPreferences(DFragment.comp19D, 0);
        comp19D = spcD.getBoolean("estado19DD", false);

        spcD = getSharedPreferences(DFragment.comp20D, 0);
        comp20D = spcD.getBoolean("estado20DD", false);

        spcD = getSharedPreferences(DFragment.comp21D, 0);
        comp21D = spcD.getBoolean("estado21DD", false);

    }

    private void letraE() {
        SharedPreferences spcE;
        spcE = getSharedPreferences(EFragment.comp1E, 0);
        comp1E = spcE.getBoolean("estado1E", false);

        spcE = getSharedPreferences(EFragment.comp2E, 0);
        comp2E = spcE.getBoolean("estado2E", false);
    }

    private void letraH() {
        SharedPreferences spcH;
        spcH = getSharedPreferences(HFragment.comp1H, 0);
        comp1H = spcH.getBoolean("estado1H", false);

        spcH = getSharedPreferences(HFragment.comp2H, 0);
        comp2H = spcH.getBoolean("estado2H", false);
    }

    private void letraI() {
        SharedPreferences spcI;
        spcI = getSharedPreferences(IFragment.comp1I, 0);
        comp1I = spcI.getBoolean("estado1I", false);

        spcI = getSharedPreferences(IFragment.comp2I, 0);
        comp2I = spcI.getBoolean("estado2I", false);

        spcI = getSharedPreferences(IFragment.comp3I, 0);
        comp3I = spcI.getBoolean("estado3I", false);
    }

    private void letraL() {
        SharedPreferences spcL;
        spcL = getSharedPreferences(LFragment.comp1L, 0);
        comp1L = spcL.getBoolean("estado1L", false);

        spcL = getSharedPreferences(LFragment.comp2L, 0);
        comp2L = spcL.getBoolean("estado2L", false);

        spcL = getSharedPreferences(LFragment.comp3L, 0);
        comp3L = spcL.getBoolean("estado3L", false);
    }

    private void letraM() {
        SharedPreferences spcM;
        spcM = getSharedPreferences(MFragment.comp1M, 0);
        comp1M = spcM.getBoolean("estado1M", false);

        spcM = getSharedPreferences(MFragment.comp2M, 0);
        comp2M = spcM.getBoolean("estado2M", false);

        spcM = getSharedPreferences(MFragment.comp3M, 0);
        comp3M = spcM.getBoolean("estado3M", false);

        spcM = getSharedPreferences(MFragment.comp4M, 0);
        comp4M = spcM.getBoolean("estado4M", false);

        spcM = getSharedPreferences(MFragment.comp5M, 0);
        comp5M = spcM.getBoolean("estado5M", false);

        spcM = getSharedPreferences(MFragment.comp6M, 0);
        comp6M = spcM.getBoolean("estado6M", false);
    }

    private void letraP() {
        SharedPreferences spcP;

        spcP = getSharedPreferences(PFragment.comp1P, 0);
        comp1P = spcP.getBoolean("estado1P", false);

        spcP = getSharedPreferences(PFragment.comp2P, 0);
        comp2P = spcP.getBoolean("estado2P", false);

        spcP = getSharedPreferences(PFragment.comp3P, 0);
        comp3P = spcP.getBoolean("estado3P", false);

        spcP = getSharedPreferences(PFragment.comp4P, 0);
        comp4P = spcP.getBoolean("estado4P", false);

        spcP = getSharedPreferences(PFragment.comp5P, 0);
        comp5P = spcP.getBoolean("estado5P", false);

        spcP = getSharedPreferences(PFragment.comp6P, 0);
        comp6P = spcP.getBoolean("estado6P", false);

        spcP = getSharedPreferences(PFragment.comp7P, 0);
        comp7P = spcP.getBoolean("estado7P", false);

        spcP = getSharedPreferences(PFragment.comp8P, 0);
        comp8P = spcP.getBoolean("estado8P", false);

        spcP = getSharedPreferences(PFragment.comp9P, 0);
        comp9P = spcP.getBoolean("estado9P", false);

        spcP = getSharedPreferences(PFragment.comp10P, 0);
        comp10P = spcP.getBoolean("estado10P", false);

        spcP = getSharedPreferences(PFragment.comp11P, 0);
        comp11P = spcP.getBoolean("estado11P", false);

        spcP = getSharedPreferences(PFragment.comp12P, 0);
        comp12P = spcP.getBoolean("estado12P", false);

        spcP = getSharedPreferences(PFragment.comp13P, 0);
        comp13P = spcP.getBoolean("estado13P", false);

        spcP = getSharedPreferences(PFragment.comp14P, 0);
        comp14P = spcP.getBoolean("estado14P", false);

        spcP = getSharedPreferences(PFragment.comp15P, 0);
        comp15P = spcP.getBoolean("estado15P", false);

        spcP = getSharedPreferences(PFragment.comp16P, 0);
        comp16P = spcP.getBoolean("estado16P", false);

        spcP = getSharedPreferences(PFragment.comp17P, 0);
        comp17P = spcP.getBoolean("estado17P", false);

        spcP = getSharedPreferences(PFragment.comp18P, 0);
        comp18P = spcP.getBoolean("estado18P", false);

        spcP = getSharedPreferences(PFragment.comp19P, 0);
        comp19P = spcP.getBoolean("estado19P", false);

        spcP = getSharedPreferences(PFragment.comp20P, 0);
        comp20P = spcP.getBoolean("estado20P", false);

        spcP = getSharedPreferences(PFragment.comp21P, 0);
        comp21P = spcP.getBoolean("estado21P", false);

        spcP = getSharedPreferences(PFragment.comp22P, 0);
        comp22P = spcP.getBoolean("estado22P", false);

        spcP = getSharedPreferences(PFragment.comp23P, 0);
        comp23P = spcP.getBoolean("estado23P", false);

        spcP = getSharedPreferences(PFragment.comp24P, 0);
        comp24P = spcP.getBoolean("estado24P", false);

        spcP = getSharedPreferences(PFragment.comp25P, 0);
        comp25P = spcP.getBoolean("estado25P", false);

        spcP = getSharedPreferences(PFragment.comp26P, 0);
        comp26P = spcP.getBoolean("estado26P", false);

        spcP = getSharedPreferences(PFragment.comp27P, 0);
        comp27P = spcP.getBoolean("estado27P", false);

        spcP = getSharedPreferences(PFragment.comp28P, 0);
        comp28P = spcP.getBoolean("estado28P", false);

        spcP = getSharedPreferences(PFragment.comp29P, 0);
        comp29P = spcP.getBoolean("estado29P", false);

        spcP = getSharedPreferences(PFragment.comp30P, 0);
        comp30P = spcP.getBoolean("estado30P", false);

        spcP = getSharedPreferences(PFragment.comp31P, 0);
        comp31P = spcP.getBoolean("estado31P", false);

        spcP = getSharedPreferences(PFragment.comp32P, 0);
        comp32P = spcP.getBoolean("estado32P", false);

        spcP = getSharedPreferences(PFragment.comp33P, 0);
        comp33P = spcP.getBoolean("estado33P", false);

        spcP = getSharedPreferences(PFragment.comp34P, 0);
        comp34P = spcP.getBoolean("estado34P", false);

        spcP = getSharedPreferences(PFragment.comp35P, 0);
        comp35P = spcP.getBoolean("estado35P", false);

        spcP = getSharedPreferences(PFragment.comp36P, 0);
        comp36P = spcP.getBoolean("estado36P", false);

        spcP = getSharedPreferences(PFragment.comp37P, 0);
        comp37P = spcP.getBoolean("estado37P", false);

        spcP = getSharedPreferences(PFragment.comp38P, 0);
        comp38P = spcP.getBoolean("estado38P", false);

        spcP = getSharedPreferences(PFragment.comp39P, 0);
        comp39P = spcP.getBoolean("estado39P", false);

        spcP = getSharedPreferences(PFragment.comp40P, 0);
        comp40P = spcP.getBoolean("estado40P", false);
    }

    private void letraS() {
        SharedPreferences spcS;
        spcS = getSharedPreferences(SFragment.comp1S, 0);
        comp1S = spcS.getBoolean("estado1S", false);

        spcS = getSharedPreferences(SFragment.comp2S, 0);
        comp2S = spcS.getBoolean("estado2S", false);

        spcS = getSharedPreferences(SFragment.comp3S, 0);
        comp3S = spcS.getBoolean("estado3S", false);

        spcS = getSharedPreferences(SFragment.comp4S, 0);
        comp4S = spcS.getBoolean("estado4S", false);

        spcS = getSharedPreferences(SFragment.comp5S, 0);
        comp5S = spcS.getBoolean("estado5S", false);

        spcS = getSharedPreferences(SFragment.comp6S, 0);
        comp6S = spcS.getBoolean("estado6S", false);

        spcS = getSharedPreferences(SFragment.comp7S, 0);
        comp7S = spcS.getBoolean("estado7S", false);

        spcS = getSharedPreferences(SFragment.comp8S, 0);
        comp8S = spcS.getBoolean("estado8S", false);

        spcS = getSharedPreferences(SFragment.comp9S, 0);
        comp9S = spcS.getBoolean("estado9S", false);

        spcS = getSharedPreferences(SFragment.comp10S, 0);
        comp10S = spcS.getBoolean("estado10S", false);

        spcS = getSharedPreferences(SFragment.comp11S, 0);
        comp11S = spcS.getBoolean("estado11S", false);

        spcS = getSharedPreferences(SFragment.comp12S, 0);
        comp12S = spcS.getBoolean("estado12S", false);

        spcS = getSharedPreferences(SFragment.comp13S, 0);
        comp13S = spcS.getBoolean("estado13S", false);

        spcS = getSharedPreferences(SFragment.comp14S, 0);
        comp14S = spcS.getBoolean("estado14S", false);

        spcS = getSharedPreferences(SFragment.comp15S, 0);
        comp15S = spcS.getBoolean("estado15S", false);

        spcS = getSharedPreferences(SFragment.comp16S, 0);
        comp16S = spcS.getBoolean("estado16S", false);

        spcS = getSharedPreferences(SFragment.comp17S, 0);
        comp17S = spcS.getBoolean("estado17S", false);

        spcS = getSharedPreferences(SFragment.comp18S, 0);
        comp18S = spcS.getBoolean("estado18S", false);

        spcS = getSharedPreferences(SFragment.comp19S, 0);
        comp19S = spcS.getBoolean("estado19S", false);

        spcS = getSharedPreferences(SFragment.comp20S, 0);
        comp20S = spcS.getBoolean("estado20S", false);

        spcS = getSharedPreferences(SFragment.comp21S, 0);
        comp21S = spcS.getBoolean("estado21S", false);

        spcS = getSharedPreferences(SFragment.comp22S, 0);
        comp22S = spcS.getBoolean("estado22S", false);
    }

    private void letraT() {
        SharedPreferences spcT;
        spcT = getSharedPreferences(TFragment.comp1T, 0);
        comp1T = spcT.getBoolean("estado1T", false);

        spcT = getSharedPreferences(TFragment.comp2T, 0);
        comp2T = spcT.getBoolean("estado2T", false);

        spcT = getSharedPreferences(TFragment.comp3T, 0);
        comp3T = spcT.getBoolean("estado3T", false);

        spcT = getSharedPreferences(TFragment.comp4T, 0);
        comp4T = spcT.getBoolean("estado4T", false);
    }

    private void letraV() {
        SharedPreferences spcV;
        spcV = getSharedPreferences(VFragment.comp1V, 0);
        comp1V = spcV.getBoolean("estado1V", false);

        spcV = getSharedPreferences(VFragment.comp2V, 0);
        comp2V = spcV.getBoolean("estado2V", false);

        spcV = getSharedPreferences(VFragment.comp3V, 0);
        comp3V = spcV.getBoolean("estado3V", false);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}