package com.gustavosg.cabelossaudaveis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Cronograma extends AppCompatActivity {

    private Button oqe;
    private Button monte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cronograma);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        oqe = (Button) findViewById(R.id.button_oqe);
        monte = (Button) findViewById(R.id.button_monte);
    }

    @Override
    protected void onStart() {
        super.onStart();

        oqe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cronograma.this, OqeCronograma.class);
                startActivity(intent);
            }
        });

        monte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cronograma.this, MonteCronograma.class);
                startActivity(intent);
            }
        });
    }
}
