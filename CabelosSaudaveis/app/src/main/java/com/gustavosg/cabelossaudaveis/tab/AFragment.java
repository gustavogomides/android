package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;

public class AFragment extends Fragment implements View.OnClickListener {
    public static final String comp1A = "componente1A";
    public static final String comp2A = "componente2A";
    public static final String comp3A = "componente3A";
    public static final String comp4A = "componente4A";
    public static final String comp5A = "componente5A";
    public static final String comp6A = "componente6A";
    public static final String comp7A = "componente7A";
    public static final String comp8A = "componente8A";
    public static final String comp9A = "componente9A";
    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;
    public CheckBox componente5;
    public CheckBox componente6;
    public CheckBox componente7;
    public CheckBox componente8;
    public CheckBox componente9;
    private SharedPreferences spc1A;
    private SharedPreferences spc2A;
    private SharedPreferences spc3A;
    private SharedPreferences spc4A;
    private SharedPreferences spc5A;
    private SharedPreferences spc6A;
    private SharedPreferences spc7A;
    private SharedPreferences spc8A;
    private SharedPreferences spc9A;
    private SharedPreferences.Editor ec1A;
    private SharedPreferences.Editor ec2A;
    private SharedPreferences.Editor ec3A;
    private SharedPreferences.Editor ec4A;
    private SharedPreferences.Editor ec5A;
    private SharedPreferences.Editor ec6A;
    private SharedPreferences.Editor ec7A;
    private SharedPreferences.Editor ec8A;
    private SharedPreferences.Editor ec9A;

    private View view;
    private boolean clicou = false;

    public AFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_a,
                container, false);

        setarCheckBox();
        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_A);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_A);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_A);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_A);
        componente5 = (CheckBox) view.findViewById(R.id.componente5_A);
        componente6 = (CheckBox) view.findViewById(R.id.componente6_A);
        componente7 = (CheckBox) view.findViewById(R.id.componente7_A);
        componente8 = (CheckBox) view.findViewById(R.id.componente8_A);
        componente9 = (CheckBox) view.findViewById(R.id.componente9_A);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.clearSharedPreferences(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);
        componente5.setOnClickListener(this);
        componente6.setOnClickListener(this);
        componente7.setOnClickListener(this);
        componente8.setOnClickListener(this);
        componente9.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }

    }

    private void setarSP() {
        spc1A = getActivity().getSharedPreferences(comp1A, 0);
        spc2A = getActivity().getSharedPreferences(comp2A, 0);
        spc3A = getActivity().getSharedPreferences(comp3A, 0);
        spc4A = getActivity().getSharedPreferences(comp4A, 0);
        spc5A = getActivity().getSharedPreferences(comp5A, 0);
        spc6A = getActivity().getSharedPreferences(comp6A, 0);
        spc7A = getActivity().getSharedPreferences(comp7A, 0);
        spc8A = getActivity().getSharedPreferences(comp8A, 0);
        spc9A = getActivity().getSharedPreferences(comp9A, 0);

        ec1A = spc1A.edit();
        ec2A = spc2A.edit();
        ec3A = spc3A.edit();
        ec4A = spc4A.edit();
        ec5A = spc5A.edit();
        ec6A = spc6A.edit();
        ec7A = spc7A.edit();
        ec8A = spc8A.edit();
        ec9A = spc9A.edit();
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_A:
                ec1A.putBoolean("estado1A", componente1.isChecked());
                ec1A.commit();
                break;
            case R.id.componente2_A:
                ec2A.putBoolean("estado2A", componente2.isChecked());
                ec2A.commit();
                break;
            case R.id.componente3_A:
                ec3A.putBoolean("estado3A", componente3.isChecked());
                ec3A.commit();
                break;
            case R.id.componente4_A:
                ec4A.putBoolean("estado4A", componente4.isChecked());
                ec4A.commit();
                break;
            case R.id.componente5_A:
                ec5A.putBoolean("estado5A", componente5.isChecked());
                ec5A.commit();
                break;
            case R.id.componente6_A:
                ec6A.putBoolean("estado6A", componente6.isChecked());
                ec6A.commit();
                break;
            case R.id.componente7_A:
                ec7A.putBoolean("estado7A", componente7.isChecked());
                ec7A.commit();
                break;
            case R.id.componente8_A:
                ec8A.putBoolean("estado8A", componente8.isChecked());
                ec8A.commit();
                break;
            case R.id.componente9_A:
                ec9A.putBoolean("estado9A", componente9.isChecked());
                ec9A.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1A.putBoolean("estado1A", false);
            ec1A.commit();

            ec2A.putBoolean("estado2A", false);
            ec2A.commit();

            ec3A.putBoolean("estado3A", false);
            ec3A.commit();

            ec4A.putBoolean("estado4A", false);
            ec4A.commit();

            ec5A.putBoolean("estado5A", false);
            ec5A.commit();

            ec6A.putBoolean("estado6A", false);
            ec6A.commit();

            ec7A.putBoolean("estado7A", false);
            ec7A.commit();

            ec8A.putBoolean("estado8A", false);
            ec8A.commit();

            ec9A.putBoolean("estado9A", false);
            ec9A.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}