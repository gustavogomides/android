package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class HFragment extends Fragment implements View.OnClickListener {
    public static final String comp1H = "componente1H";
    public static final String comp2H = "componente2H";
    public CheckBox componente1;
    public CheckBox componente2;
    private SharedPreferences spc1H;
    private SharedPreferences spc2H;

    private SharedPreferences.Editor ec1H;
    private SharedPreferences.Editor ec2H;

    private boolean clicou = false;

    private View view;

    public HFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_h,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_H);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_H);
    }

    private void setarSP() {
        spc1H = getActivity().getSharedPreferences(comp1H, 0);
        spc2H = getActivity().getSharedPreferences(comp2H, 0);

        ec1H = spc1H.edit();
        ec2H = spc2H.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_H:
                ec1H.putBoolean("estado1H", componente1.isChecked());
                ec1H.commit();
                break;
            case R.id.componente2_H:
                ec2H.putBoolean("estado2H", componente2.isChecked());
                ec2H.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1H.putBoolean("estado1H", false);
            ec1H.commit();

            ec2H.putBoolean("estado2H", false);
            ec2H.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

}