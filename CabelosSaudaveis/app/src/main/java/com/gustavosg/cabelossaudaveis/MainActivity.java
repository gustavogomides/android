package com.gustavosg.cabelossaudaveis;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static ProgressDialog progress;
    private TextView tv_componentes;
    private TextView tv_produtos;
    private TextView tv_cronograma;
    private TextView tv_capilar;
    private TextView tv_cabelos;
    private TextView tv_saudaveis;
    private TextView tv_tipode;
    private TextView tv_cabelo;
    private TextView tv_receitas;
    private TextView tv_caseiras;
    private TextView tv_extras;
    private ImageButton button_componentes;
    private ImageButton button_cronograma;
    private Typeface font;
    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        font = Typeface.createFromAsset(getAssets(), "segoepr.ttf");

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);

            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setTypeface(font);
                textView.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);
            }
        }
        setarFonte();

        setarObjetos();
    }

    private void setarObjetos() {
        button_componentes = (ImageButton) findViewById(R.id.button_componentes);
        button_cronograma = (ImageButton) findViewById(R.id.button_cronograma);
    }

    private void setarFonte() {

        tv_cabelos = (TextView) findViewById(R.id.titulo_cabelos);
        tv_cabelos.setTypeface(font);
        tv_cabelos.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_saudaveis = (TextView) findViewById(R.id.titulo_saudaveis);
        tv_saudaveis.setTypeface(font);
        tv_saudaveis.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_componentes = (TextView) findViewById(R.id.tv_componentes);
        tv_componentes.setTypeface(font);
        tv_componentes.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_produtos = (TextView) findViewById(R.id.tv_produtos);
        tv_produtos.setTypeface(font);
        tv_produtos.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_cronograma = (TextView) findViewById(R.id.tv_cronograma);
        tv_cronograma.setTypeface(font);
        tv_cronograma.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_capilar = (TextView) findViewById(R.id.tv_capilar);
        tv_capilar.setTypeface(font);
        tv_capilar.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_tipode = (TextView) findViewById(R.id.tv_tipode);
        tv_tipode.setTypeface(font);
        tv_tipode.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_cabelo = (TextView) findViewById(R.id.tv_cabelo);
        tv_cabelo.setTypeface(font);
        tv_cabelo.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_receitas = (TextView) findViewById(R.id.tv_receitas);
        tv_receitas.setTypeface(font);
        tv_receitas.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_caseiras = (TextView) findViewById(R.id.tv_caseiras);
        tv_caseiras.setTypeface(font);
        tv_caseiras.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

        tv_extras = (TextView) findViewById(R.id.tv_extras);
        tv_extras.setTypeface(font);
        tv_extras.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);

    }

    @Override
    protected void onStart() {
        super.onStart();

        button_componentes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Componentes.class);
                startActivity(intent);
            }
        });

        button_cronograma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Cronograma.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_tutorial) {
            return true;
        } else if (id == R.id.menu_temas) {

        } else if (id == R.id.menu_informarerro) {

        } else if (id == R.id.menu_compartilharapp) {
            compartilhar();

        } else if (id == R.id.menu_avaliar) {
            avaliar();
        } else if (id == R.id.menu_sobre) {
            sobre();
        }


        return super.onOptionsItemSelected(item);
    }

    private void compartilhar() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object;
        final String COMPARTILHAR = getResources().getString(R.string.textocompartilhar) + " http://market.android.com/details?id=" + appPackageName;
        final String SHARE = getResources().getString(R.string.enviarvia);
        final String SUBJECT = getResources().getString(R.string.subject);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT);
        emailIntent.putExtra(Intent.EXTRA_TEXT, COMPARTILHAR);
        startActivity(Intent.createChooser(emailIntent, SHARE + "\n"));
    }

    private void avaliar() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object;
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void sobre() {

        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //define o titulo
        builder.setTitle("Cabelos Saudáveis");
        //define a mensagem
        builder.setMessage(getResources().getString(R.string.versao) + "\n\nDesenvolvedor: Gustavo Gomides");
        //define um botão como positivo


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                alerta.dismiss();
            }
        });

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }


    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(MainActivity activity) {
            //dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            /*dialog.setMessage("Carregando Componentes...");
            dialog.show();*/
        }

        @Override
        protected void onPostExecute(Void result) {
            //dialog.cancel();
        }

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

    }

}
