package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class IFragment extends Fragment implements View.OnClickListener {
    public static final String comp1I = "componente1I";
    public static final String comp2I = "componente2I";
    public static final String comp3I = "componente3I";
    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    private SharedPreferences spc1I;
    private SharedPreferences spc2I;
    private SharedPreferences spc3I;

    private SharedPreferences.Editor ec1I;
    private SharedPreferences.Editor ec2I;
    private SharedPreferences.Editor ec3I;

    private boolean clicou = false;

    private View view;

    public IFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_i,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_I);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_I);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_I);
    }

    private void setarSP() {
        spc1I = getActivity().getSharedPreferences(comp1I, 0);
        spc2I = getActivity().getSharedPreferences(comp2I, 0);
        spc3I = getActivity().getSharedPreferences(comp3I, 0);

        ec1I = spc1I.edit();
        ec2I = spc2I.edit();
        ec3I = spc3I.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_I:
                ec1I.putBoolean("estado1I", componente1.isChecked());
                ec1I.commit();
                break;
            case R.id.componente2_I:
                ec2I.putBoolean("estado2I", componente2.isChecked());
                ec2I.commit();
                break;
            case R.id.componente3_I:
                ec3I.putBoolean("estado3I", componente3.isChecked());
                ec3I.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1I.putBoolean("estado1I", false);
            ec1I.commit();

            ec2I.putBoolean("estado2I", false);
            ec2I.commit();

            ec3I.putBoolean("estado3I", false);
            ec3I.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

}