package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class VFragment extends Fragment implements View.OnClickListener {
    public static final String comp1V = "componente1V";
    public static final String comp2V = "componente2V";
    public static final String comp3V = "componente3V";

    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;

    private SharedPreferences spc1V;
    private SharedPreferences spc2V;
    private SharedPreferences spc3V;

    private SharedPreferences.Editor ec1V;
    private SharedPreferences.Editor ec2V;
    private SharedPreferences.Editor ec3V;

    private boolean clicou = false;

    private View view;

    public VFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_v,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_V);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_V);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_V);
    }

    private void setarSP() {
        spc1V = getActivity().getSharedPreferences(comp1V, 0);
        spc2V = getActivity().getSharedPreferences(comp2V, 0);
        spc3V = getActivity().getSharedPreferences(comp3V, 0);

        ec1V = spc1V.edit();
        ec2V = spc2V.edit();
        ec3V = spc3V.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_V:
                ec1V.putBoolean("estado1V", componente1.isChecked());
                ec1V.commit();
                break;
            case R.id.componente2_V:
                ec2V.putBoolean("estado2V", componente2.isChecked());
                ec2V.commit();
                break;
            case R.id.componente3_V:
                ec3V.putBoolean("estado3V", componente3.isChecked());
                ec3V.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1V.putBoolean("estado1V", false);
            ec1V.commit();

            ec2V.putBoolean("estado2V", false);
            ec2V.commit();

            ec3V.putBoolean("estado3V", false);
            ec3V.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}