package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class EFragment extends Fragment implements View.OnClickListener {
    public static final String comp1E = "componente1E";
    public static final String comp2E = "componente2E";
    public CheckBox componente1;
    public CheckBox componente2;
    private SharedPreferences spc1E;
    private SharedPreferences spc2E;

    private SharedPreferences.Editor ec1E;
    private SharedPreferences.Editor ec2E;

    private boolean clicou = false;

    private View view;

    public EFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_e,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_E);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_E);
    }

    private void setarSP() {
        spc1E = getActivity().getSharedPreferences(comp1E, 0);
        spc2E = getActivity().getSharedPreferences(comp2E, 0);

        ec1E = spc1E.edit();
        ec2E = spc2E.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_E:
                ec1E.putBoolean("estado1E", componente1.isChecked());
                ec1E.commit();
                break;
            case R.id.componente2_E:
                ec2E.putBoolean("estado2E", componente2.isChecked());
                ec2E.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1E.putBoolean("estado1E", false);
            ec1E.commit();

            ec2E.putBoolean("estado2E", false);
            ec2E.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

}