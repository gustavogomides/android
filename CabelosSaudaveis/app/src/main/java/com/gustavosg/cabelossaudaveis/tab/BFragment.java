package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class BFragment extends Fragment implements View.OnClickListener {
    public static final String comp1B = "componente1B";
    public static final String comp2B = "componente2B";
    public CheckBox componente1;
    public CheckBox componente2;
    private SharedPreferences spc1B;
    private SharedPreferences spc2B;

    private SharedPreferences.Editor ec1B;
    private SharedPreferences.Editor ec2B;

    private boolean clicou = false;

    private View view;

    public BFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_b,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_B);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_B);
    }

    private void setarSP() {
        spc1B = getActivity().getSharedPreferences(comp1B, 0);
        spc2B = getActivity().getSharedPreferences(comp2B, 0);

        ec1B = spc1B.edit();
        ec2B = spc2B.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_B:
                ec1B.putBoolean("estado1B", componente1.isChecked());
                ec1B.commit();
                break;
            case R.id.componente2_B:
                ec2B.putBoolean("estado2B", componente2.isChecked());
                ec2B.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1B.putBoolean("estado1B", false);
            ec1B.commit();

            ec2B.putBoolean("estado2B", false);
            ec2B.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

}