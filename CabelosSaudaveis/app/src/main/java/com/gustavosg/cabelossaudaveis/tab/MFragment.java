package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;

public class MFragment extends Fragment implements View.OnClickListener {
    public static final String comp1M = "componente1M";
    public static final String comp2M = "componente2M";
    public static final String comp3M = "componente3M";
    public static final String comp4M = "componente4M";
    public static final String comp5M = "componente5M";
    public static final String comp6M = "componente6M";
    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;
    public CheckBox componente5;
    public CheckBox componente6;
    private SharedPreferences spc1M;
    private SharedPreferences spc2M;
    private SharedPreferences spc3M;
    private SharedPreferences spc4M;
    private SharedPreferences spc5M;
    private SharedPreferences spc6M;
    private SharedPreferences.Editor ec1M;
    private SharedPreferences.Editor ec2M;
    private SharedPreferences.Editor ec3M;
    private SharedPreferences.Editor ec4M;
    private SharedPreferences.Editor ec5M;
    private SharedPreferences.Editor ec6M;
    private View view;
    private boolean clicou = false;

    public MFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_m,
                container, false);

        setarCheckBox();
        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_M);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_M);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_M);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_M);
        componente5 = (CheckBox) view.findViewById(R.id.componente5_M);
        componente6 = (CheckBox) view.findViewById(R.id.componente6_M);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.clearSharedPreferences(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();
        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);
        componente5.setOnClickListener(this);
        componente6.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }

    }

    private void setarSP() {
        spc1M = getActivity().getSharedPreferences(comp1M, 0);
        spc2M = getActivity().getSharedPreferences(comp2M, 0);
        spc3M = getActivity().getSharedPreferences(comp3M, 0);
        spc4M = getActivity().getSharedPreferences(comp4M, 0);
        spc5M = getActivity().getSharedPreferences(comp5M, 0);
        spc6M = getActivity().getSharedPreferences(comp6M, 0);

        ec1M = spc1M.edit();
        ec2M = spc2M.edit();
        ec3M = spc3M.edit();
        ec4M = spc4M.edit();
        ec5M = spc5M.edit();
        ec6M = spc6M.edit();
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_M:
                ec1M.putBoolean("estado1M", componente1.isChecked());
                ec1M.commit();
                break;
            case R.id.componente2_M:
                ec2M.putBoolean("estado2M", componente2.isChecked());
                ec2M.commit();
                break;
            case R.id.componente3_M:
                ec3M.putBoolean("estado3M", componente3.isChecked());
                ec3M.commit();
                break;
            case R.id.componente4_M:
                ec4M.putBoolean("estado4M", componente4.isChecked());
                ec4M.commit();
                break;
            case R.id.componente5_M:
                ec5M.putBoolean("estado5M", componente5.isChecked());
                ec5M.commit();
                break;
            case R.id.componente6_M:
                ec6M.putBoolean("estado6M", componente6.isChecked());
                ec6M.commit();
                break;

        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1M.putBoolean("estado1M", false);
            ec1M.commit();

            ec2M.putBoolean("estado2M", false);
            ec2M.commit();

            ec3M.putBoolean("estado3M", false);
            ec3M.commit();

            ec4M.putBoolean("estado4M", false);
            ec4M.commit();

            ec5M.putBoolean("estado5M", false);
            ec5M.commit();

            ec6M.putBoolean("estado6M", false);
            ec6M.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}