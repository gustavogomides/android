package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class TFragment extends Fragment implements View.OnClickListener {
    public static final String comp1T = "componente1T";
    public static final String comp2T = "componente2T";
    public static final String comp3T = "componente3T";
    public static final String comp4T = "componente4T";

    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;

    private SharedPreferences spc1T;
    private SharedPreferences spc2T;
    private SharedPreferences spc3T;
    private SharedPreferences spc4T;

    private SharedPreferences.Editor ec1T;
    private SharedPreferences.Editor ec2T;
    private SharedPreferences.Editor ec3T;
    private SharedPreferences.Editor ec4T;

    private boolean clicou = false;

    private View view;

    public TFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_t,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_T);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_T);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_T);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_T);
    }

    private void setarSP() {
        spc1T = getActivity().getSharedPreferences(comp1T, 0);
        spc2T = getActivity().getSharedPreferences(comp2T, 0);
        spc3T = getActivity().getSharedPreferences(comp3T, 0);
        spc4T = getActivity().getSharedPreferences(comp4T, 0);

        ec1T = spc1T.edit();
        ec2T = spc2T.edit();
        ec3T = spc3T.edit();
        ec4T = spc4T.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_T:
                ec1T.putBoolean("estado1T", componente1.isChecked());
                ec1T.commit();
                break;
            case R.id.componente2_T:
                ec2T.putBoolean("estado2T", componente2.isChecked());
                ec2T.commit();
                break;
            case R.id.componente3_T:
                ec3T.putBoolean("estado3T", componente3.isChecked());
                ec3T.commit();
                break;
            case R.id.componente4_T:
                ec4T.putBoolean("estado4T", componente4.isChecked());
                ec4T.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1T.putBoolean("estado1T", false);
            ec1T.commit();

            ec2T.putBoolean("estado2T", false);
            ec2T.commit();

            ec3T.putBoolean("estado3T", false);
            ec3T.commit();

            ec4T.putBoolean("estado4T", false);
            ec4T.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}