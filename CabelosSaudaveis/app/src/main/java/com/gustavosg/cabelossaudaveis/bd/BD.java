package com.gustavosg.cabelossaudaveis.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BD {
    private SQLiteDatabase bd;
    private BDCore auxBd;

    public BD(Context context) {
        auxBd = new BDCore(context);
        bd = auxBd.getWritableDatabase();
    }

    public void desconectar() {
        SQLiteDatabase db = auxBd.getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    private String getDate(Date dia) {
        Log.i("DIA BD", dia + "");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(dia);
    }

    public long inserir(ModeloCronograma cronograma) {
        //// clique string, categoria string, dia date, cor string
        ContentValues valores = new ContentValues();
        valores.put("clique", cronograma.getClique());
        valores.put("categoria", cronograma.getCategoria());
        valores.put("dia", getDate(cronograma.getDia()));
        valores.put("cor", cronograma.getCor());

        return bd.insert("cronograma", null, valores);
    }

    /*public ArrayList<Dieta> dietasCadastradas() {
        ArrayList<Dieta> arrayListDieta = new ArrayList<>();

        String sql = "SELECT * from dieta";
        Cursor cursor = bd.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            return null;
        } else if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                arrayListDieta.add(new Dieta(cursor.getString(0), cursor.getString(1), cursor.getInt(2)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arrayListDieta;
    }*/

    public boolean deletarAluno(String hora, String min, String racao) {
        String table = "dieta";
        String where = "hora = '" + hora + "' and minuto = '" + min + "' and racao = '" + racao + "'";
        return bd.delete(table, where, null) != 0;
    }
}

