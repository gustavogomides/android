package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;

public class CFragment extends Fragment implements View.OnClickListener {
    public static final String comp1C = "componente1C";
    public static final String comp2C = "componente2C";
    public static final String comp3C = "componente3C";
    public static final String comp4C = "componente4C";
    public static final String comp5C = "componente5C";
    public static final String comp6C = "componente6C";
    public static final String comp7C = "componente7C";
    public static final String comp8C = "componente8C";
    public static final String comp9C = "componente9C";
    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;
    public CheckBox componente5;
    public CheckBox componente6;
    public CheckBox componente7;
    public CheckBox componente8;
    public CheckBox componente9;
    private SharedPreferences spc1C;
    private SharedPreferences spc2C;
    private SharedPreferences spc3C;
    private SharedPreferences spc4C;
    private SharedPreferences spc5C;
    private SharedPreferences spc6C;
    private SharedPreferences spc7C;
    private SharedPreferences spc8C;
    private SharedPreferences spc9C;
    private SharedPreferences.Editor ec1C;
    private SharedPreferences.Editor ec2C;
    private SharedPreferences.Editor ec3C;
    private SharedPreferences.Editor ec4C;
    private SharedPreferences.Editor ec5C;
    private SharedPreferences.Editor ec6C;
    private SharedPreferences.Editor ec7C;
    private SharedPreferences.Editor ec8C;
    private SharedPreferences.Editor ec9C;
    private View view;
    private boolean clicou = false;

    public CFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_c,
                container, false);

        setarCheckBox();
        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_C);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_C);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_C);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_C);
        componente5 = (CheckBox) view.findViewById(R.id.componente5_C);
        componente6 = (CheckBox) view.findViewById(R.id.componente6_C);
        componente7 = (CheckBox) view.findViewById(R.id.componente7_C);
        componente8 = (CheckBox) view.findViewById(R.id.componente8_C);
        componente9 = (CheckBox) view.findViewById(R.id.componente9_C);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();
        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);
        componente5.setOnClickListener(this);
        componente6.setOnClickListener(this);
        componente7.setOnClickListener(this);
        componente8.setOnClickListener(this);
        componente9.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }

    }

    private void setarSP() {
        spc1C = getActivity().getSharedPreferences(comp1C, 0);
        spc2C = getActivity().getSharedPreferences(comp2C, 0);
        spc3C = getActivity().getSharedPreferences(comp3C, 0);
        spc4C = getActivity().getSharedPreferences(comp4C, 0);
        spc5C = getActivity().getSharedPreferences(comp5C, 0);
        spc6C = getActivity().getSharedPreferences(comp6C, 0);
        spc7C = getActivity().getSharedPreferences(comp7C, 0);
        spc8C = getActivity().getSharedPreferences(comp8C, 0);
        spc9C = getActivity().getSharedPreferences(comp9C, 0);

        ec1C = spc1C.edit();
        ec2C = spc2C.edit();
        ec3C = spc3C.edit();
        ec4C = spc4C.edit();
        ec5C = spc5C.edit();
        ec6C = spc6C.edit();
        ec7C = spc7C.edit();
        ec8C = spc8C.edit();
        ec9C = spc9C.edit();
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_C:
                ec1C.putBoolean("estado1C", componente1.isChecked());
                ec1C.commit();
                break;
            case R.id.componente2_C:
                ec2C.putBoolean("estado2C", componente2.isChecked());
                ec2C.commit();
                break;
            case R.id.componente3_C:
                ec3C.putBoolean("estado3C", componente3.isChecked());
                ec3C.commit();
                break;
            case R.id.componente4_C:
                ec4C.putBoolean("estado4C", componente4.isChecked());
                ec4C.commit();
                break;
            case R.id.componente5_C:
                ec5C.putBoolean("estado5C", componente5.isChecked());
                ec5C.commit();
                break;
            case R.id.componente6_C:
                ec6C.putBoolean("estado6C", componente6.isChecked());
                ec6C.commit();
                break;
            case R.id.componente7_C:
                ec7C.putBoolean("estado7C", componente7.isChecked());
                ec7C.commit();
                break;
            case R.id.componente8_C:
                ec8C.putBoolean("estado8C", componente8.isChecked());
                ec8C.commit();
                break;
            case R.id.componente9_C:
                ec9C.putBoolean("estado9C", componente9.isChecked());
                ec9C.commit();
                break;

        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1C.putBoolean("estado1C", false);
            ec1C.commit();

            ec2C.putBoolean("estado2C", false);
            ec2C.commit();

            ec3C.putBoolean("estado3C", false);
            ec3C.commit();

            ec4C.putBoolean("estado4C", false);
            ec4C.commit();

            ec5C.putBoolean("estado5C", false);
            ec5C.commit();

            ec6C.putBoolean("estado6C", false);
            ec6C.commit();

            ec7C.putBoolean("estado7C", false);
            ec7C.commit();

            ec8C.putBoolean("estado8C", false);
            ec8C.commit();

            ec9C.putBoolean("estado9C", false);
            ec9C.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

}