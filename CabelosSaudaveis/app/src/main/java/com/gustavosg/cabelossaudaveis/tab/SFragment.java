package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;

public class SFragment extends Fragment implements View.OnClickListener {

    public static final String comp1S = "componente1S";
    public static final String comp2S = "componente2S";
    public static final String comp3S = "componente3S";
    public static final String comp4S = "componente4S";
    public static final String comp5S = "componente5S";
    public static final String comp6S = "componente6S";
    public static final String comp7S = "componente7S";
    public static final String comp8S = "componente8S";
    public static final String comp9S = "componente9SD";
    public static final String comp10S = "componente10S";
    public static final String comp11S = "componente11S";
    public static final String comp12S = "componente12S";
    public static final String comp13S = "componente13S";
    public static final String comp14S = "componente14S";
    public static final String comp15S = "componente15S";
    public static final String comp16S = "componente16S";
    public static final String comp17S = "componente17S";
    public static final String comp18S = "componente18S";
    public static final String comp19S = "componente19S";
    public static final String comp20S = "componente20S";
    public static final String comp21S = "componente21S";
    public static final String comp22S = "componente22S";

    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    public CheckBox componente4;
    public CheckBox componente5;
    public CheckBox componente6;
    public CheckBox componente7;
    public CheckBox componente8;
    public CheckBox componente9;
    public CheckBox componente10;
    public CheckBox componente11;
    public CheckBox componente12;
    public CheckBox componente13;
    public CheckBox componente14;
    public CheckBox componente15;
    public CheckBox componente16;
    public CheckBox componente17;
    public CheckBox componente18;
    public CheckBox componente19;
    public CheckBox componente20;
    public CheckBox componente21;
    public CheckBox componente22;

    private View view;

    private SharedPreferences spc1S;
    private SharedPreferences spc2S;
    private SharedPreferences spc3S;
    private SharedPreferences spc4S;
    private SharedPreferences spc5S;
    private SharedPreferences spc6S;
    private SharedPreferences spc7S;
    private SharedPreferences spc8S;
    private SharedPreferences spc9S;
    private SharedPreferences spc10S;
    private SharedPreferences spc11S;
    private SharedPreferences spc12S;
    private SharedPreferences spc13S;
    private SharedPreferences spc14S;
    private SharedPreferences spc15S;
    private SharedPreferences spc16S;
    private SharedPreferences spc17S;
    private SharedPreferences spc18S;
    private SharedPreferences spc19S;
    private SharedPreferences spc20S;
    private SharedPreferences spc21S;
    private SharedPreferences spc22S;

    private SharedPreferences.Editor ec1S;
    private SharedPreferences.Editor ec2S;
    private SharedPreferences.Editor ec3S;
    private SharedPreferences.Editor ec4S;
    private SharedPreferences.Editor ec5S;
    private SharedPreferences.Editor ec6S;
    private SharedPreferences.Editor ec7S;
    private SharedPreferences.Editor ec8S;
    private SharedPreferences.Editor ec9S;
    private SharedPreferences.Editor ec10S;
    private SharedPreferences.Editor ec11S;
    private SharedPreferences.Editor ec12S;
    private SharedPreferences.Editor ec13S;
    private SharedPreferences.Editor ec14S;
    private SharedPreferences.Editor ec15S;
    private SharedPreferences.Editor ec16S;
    private SharedPreferences.Editor ec17S;
    private SharedPreferences.Editor ec18S;
    private SharedPreferences.Editor ec19S;
    private SharedPreferences.Editor ec20S;
    private SharedPreferences.Editor ec21S;
    private SharedPreferences.Editor ec22S;

    private boolean clicou = false;

    public SFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_s,
                container, false);

        setarCheckBox();
        return view;
    }

    private void setarSP() {
        spc1S = getActivity().getSharedPreferences(comp1S, 0);
        spc2S = getActivity().getSharedPreferences(comp2S, 0);
        spc3S = getActivity().getSharedPreferences(comp3S, 0);
        spc4S = getActivity().getSharedPreferences(comp4S, 0);
        spc5S = getActivity().getSharedPreferences(comp5S, 0);
        spc6S = getActivity().getSharedPreferences(comp6S, 0);
        spc7S = getActivity().getSharedPreferences(comp7S, 0);
        spc8S = getActivity().getSharedPreferences(comp8S, 0);
        spc9S = getActivity().getSharedPreferences(comp9S, 0);
        spc10S = getActivity().getSharedPreferences(comp10S, 0);
        spc11S = getActivity().getSharedPreferences(comp11S, 0);
        spc12S = getActivity().getSharedPreferences(comp12S, 0);
        spc13S = getActivity().getSharedPreferences(comp13S, 0);
        spc14S = getActivity().getSharedPreferences(comp14S, 0);
        spc15S = getActivity().getSharedPreferences(comp15S, 0);
        spc16S = getActivity().getSharedPreferences(comp16S, 0);
        spc17S = getActivity().getSharedPreferences(comp17S, 0);
        spc18S = getActivity().getSharedPreferences(comp18S, 0);
        spc19S = getActivity().getSharedPreferences(comp19S, 0);
        spc20S = getActivity().getSharedPreferences(comp20S, 0);
        spc21S = getActivity().getSharedPreferences(comp21S, 0);
        spc22S = getActivity().getSharedPreferences(comp22S, 0);

        ec1S = spc1S.edit();
        ec2S = spc2S.edit();
        ec3S = spc3S.edit();
        ec4S = spc4S.edit();
        ec5S = spc5S.edit();
        ec6S = spc6S.edit();
        ec7S = spc7S.edit();
        ec8S = spc8S.edit();
        ec9S = spc9S.edit();
        ec10S = spc10S.edit();
        ec11S = spc11S.edit();
        ec12S = spc12S.edit();
        ec13S = spc13S.edit();
        ec14S = spc14S.edit();
        ec15S = spc15S.edit();
        ec16S = spc16S.edit();
        ec17S = spc17S.edit();
        ec18S = spc18S.edit();
        ec19S = spc19S.edit();
        ec20S = spc20S.edit();
        ec21S = spc21S.edit();
        ec22S = spc22S.edit();
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_S);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_S);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_S);
        componente4 = (CheckBox) view.findViewById(R.id.componente4_S);
        componente5 = (CheckBox) view.findViewById(R.id.componente5_S);
        componente6 = (CheckBox) view.findViewById(R.id.componente6_S);
        componente7 = (CheckBox) view.findViewById(R.id.componente7_S);
        componente8 = (CheckBox) view.findViewById(R.id.componente8_S);
        componente9 = (CheckBox) view.findViewById(R.id.componente9_S);
        componente10 = (CheckBox) view.findViewById(R.id.componente10_S);
        componente11 = (CheckBox) view.findViewById(R.id.componente11_S);
        componente12 = (CheckBox) view.findViewById(R.id.componente12_S);
        componente13 = (CheckBox) view.findViewById(R.id.componente13_S);
        componente14 = (CheckBox) view.findViewById(R.id.componente14_S);
        componente15 = (CheckBox) view.findViewById(R.id.componente15_S);
        componente16 = (CheckBox) view.findViewById(R.id.componente16_S);
        componente17 = (CheckBox) view.findViewById(R.id.componente17_S);
        componente18 = (CheckBox) view.findViewById(R.id.componente18_S);
        componente19 = (CheckBox) view.findViewById(R.id.componente19_S);
        componente20 = (CheckBox) view.findViewById(R.id.componente20_S);
        componente21 = (CheckBox) view.findViewById(R.id.componente21_S);
        componente22 = (CheckBox) view.findViewById(R.id.componente22_S);
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);
        componente4.setOnClickListener(this);
        componente5.setOnClickListener(this);
        componente6.setOnClickListener(this);
        componente7.setOnClickListener(this);
        componente8.setOnClickListener(this);
        componente9.setOnClickListener(this);
        componente10.setOnClickListener(this);
        componente11.setOnClickListener(this);
        componente12.setOnClickListener(this);
        componente13.setOnClickListener(this);
        componente14.setOnClickListener(this);
        componente15.setOnClickListener(this);
        componente16.setOnClickListener(this);
        componente17.setOnClickListener(this);
        componente18.setOnClickListener(this);
        componente19.setOnClickListener(this);
        componente20.setOnClickListener(this);
        componente21.setOnClickListener(this);
        componente22.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_S:
                ec1S.putBoolean("estado1S", componente1.isChecked());
                ec1S.commit();
                break;
            case R.id.componente2_S:
                ec2S.putBoolean("estado2S", componente2.isChecked());
                ec2S.commit();
                break;
            case R.id.componente3_S:
                ec3S.putBoolean("estado3S", componente3.isChecked());
                ec3S.commit();
                break;
            case R.id.componente4_S:
                ec4S.putBoolean("estado4S", componente4.isChecked());
                ec4S.commit();
                break;
            case R.id.componente5_S:
                ec5S.putBoolean("estado5S", componente5.isChecked());
                ec5S.commit();
                break;
            case R.id.componente6_S:
                ec6S.putBoolean("estado6S", componente6.isChecked());
                ec6S.commit();
                break;
            case R.id.componente7_S:
                ec7S.putBoolean("estado7S", componente7.isChecked());
                ec7S.commit();
                break;
            case R.id.componente8_S:
                ec8S.putBoolean("estado8S", componente8.isChecked());
                ec8S.commit();
                break;
            case R.id.componente9_S:
                ec9S.putBoolean("estado9S", componente9.isChecked());
                ec9S.commit();
                break;
            case R.id.componente10_S:
                ec10S.putBoolean("estado10S", componente10.isChecked());
                ec10S.commit();
                break;

            case R.id.componente11_S:
                ec11S.putBoolean("estado11S", componente11.isChecked());
                ec11S.commit();
                break;

            case R.id.componente12_S:
                ec12S.putBoolean("estado12S", componente12.isChecked());
                ec12S.commit();
                break;

            case R.id.componente13_S:
                ec13S.putBoolean("estado13S", componente13.isChecked());
                ec13S.commit();
                break;

            case R.id.componente14_S:
                ec14S.putBoolean("estado14S", componente14.isChecked());
                ec14S.commit();
                break;

            case R.id.componente15_S:
                ec15S.putBoolean("estado15S", componente15.isChecked());
                ec15S.commit();
                break;

            case R.id.componente16_S:
                ec16S.putBoolean("estado16S", componente16.isChecked());
                ec16S.commit();
                break;

            case R.id.componente17_S:
                ec17S.putBoolean("estado17S", componente17.isChecked());
                ec17S.commit();
                break;

            case R.id.componente18_S:
                ec18S.putBoolean("estado18S", componente18.isChecked());
                ec18S.commit();
                break;

            case R.id.componente19_S:
                ec19S.putBoolean("estado19S", componente19.isChecked());
                ec19S.commit();
                break;

            case R.id.componente20_S:
                ec20S.putBoolean("estado20S", componente20.isChecked());
                ec20S.commit();
                break;

            case R.id.componente21_S:
                ec21S.putBoolean("estado21S", componente21.isChecked());
                ec21S.commit();
                break;

            case R.id.componente22_S:
                ec22S.putBoolean("estado22S", componente22.isChecked());
                ec22S.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1S.putBoolean("estado1S", false);
            ec1S.commit();

            ec2S.putBoolean("estado2S", false);
            ec2S.commit();

            ec3S.putBoolean("estado3S", false);
            ec3S.commit();

            ec4S.putBoolean("estado4S", false);
            ec4S.commit();

            ec5S.putBoolean("estado5S", false);
            ec5S.commit();

            ec6S.putBoolean("estado6S", false);
            ec6S.commit();

            ec7S.putBoolean("estado7S", false);
            ec7S.commit();

            ec8S.putBoolean("estado8S", false);
            ec8S.commit();

            ec9S.putBoolean("estado9S", false);
            ec9S.commit();

            ec10S.putBoolean("estado10S", false);
            ec10S.commit();

            ec11S.putBoolean("estado11S", false);
            ec11S.commit();

            ec12S.putBoolean("estado12S", false);
            ec12S.commit();

            ec13S.putBoolean("estado13S", false);
            ec13S.commit();

            ec14S.putBoolean("estado14S", false);
            ec14S.commit();

            ec15S.putBoolean("estado15S", false);
            ec15S.commit();

            ec16S.putBoolean("estado16S", false);
            ec16S.commit();

            ec17S.putBoolean("estado17S", false);
            ec17S.commit();

            ec18S.putBoolean("estado18S", false);
            ec18S.commit();

            ec19S.putBoolean("estado19S", false);
            ec19S.commit();

            ec20S.putBoolean("estado20S", false);
            ec20S.commit();

            ec21S.putBoolean("estado21S", false);
            ec21S.commit();

            ec22S.putBoolean("estado22S", false);
            ec22S.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}