package com.gustavosg.cabelossaudaveis;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gustavosg.cabelossaudaveis.bd.BD;
import com.gustavosg.cabelossaudaveis.bd.ModeloCronograma;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MonteCronograma extends AppCompatActivity implements View.OnClickListener {

    private TextView op1_1, op1_2, op1_3;
    private TextView op2_1, op2_2, op2_3;
    private TextView op3_1, op3_2, op3_3;
    private TextView op4_1, op4_2, op4_3;

    private boolean bop1_1 = false, bop1_2 = false, bop1_3 = false;
    private boolean bop2_1 = false, bop2_2 = false, bop2_3 = false;
    private boolean bop3_1 = false, bop3_2 = false, bop3_3 = false;
    private boolean bop4_1 = false, bop4_2 = false, bop4_3 = false;

    private TextView data;
    private TextView hora;
    private String strdata;
    private String strhorario;
    private boolean booleanH = true;
    private boolean booleanN = false;
    private boolean booleanR = false;
    private int corEscolhida;
    private Spinner spinnerCor;
    private TextView tvcor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monte_cronograma);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        op1_1 = (TextView) findViewById(R.id.op1_1);
        op1_2 = (TextView) findViewById(R.id.op1_2);
        op1_3 = (TextView) findViewById(R.id.op1_3);

        op2_1 = (TextView) findViewById(R.id.op2_1);
        op2_2 = (TextView) findViewById(R.id.op2_2);
        op2_3 = (TextView) findViewById(R.id.op2_3);

        op3_1 = (TextView) findViewById(R.id.op3_1);
        op3_2 = (TextView) findViewById(R.id.op3_2);
        op3_3 = (TextView) findViewById(R.id.op3_3);

        op4_1 = (TextView) findViewById(R.id.op4_1);
        op4_2 = (TextView) findViewById(R.id.op4_2);
        op4_3 = (TextView) findViewById(R.id.op4_3);
    }


    @Override
    public void onStart() {
        super.onStart();

        op1_1.setOnClickListener(this);
        op1_2.setOnClickListener(this);
        op1_3.setOnClickListener(this);

        op2_1.setOnClickListener(this);
        op2_2.setOnClickListener(this);
        op2_3.setOnClickListener(this);

        op3_1.setOnClickListener(this);
        op3_2.setOnClickListener(this);
        op3_3.setOnClickListener(this);

        op4_1.setOnClickListener(this);
        op4_2.setOnClickListener(this);
        op4_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.op1_1:
                bop1_1 = true;
                bop1_2 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op1_2:
                bop1_2 = true;
                bop1_1 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op1_3:
                bop1_3 = true;
                bop1_2 = bop1_1 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;

            case R.id.op2_1:
                bop2_1 = true;
                bop1_2 = bop1_3 = bop1_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op2_2:
                bop2_2 = true;
                bop1_1 = bop1_3 = bop2_1 = bop1_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op2_3:
                bop2_3 = true;
                bop1_2 = bop1_1 = bop2_1 = bop2_2 = bop1_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;


            case R.id.op3_1:
                bop3_1 = true;
                bop1_2 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop1_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op3_2:
                bop3_2 = true;
                bop1_1 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop1_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op3_3:
                bop3_3 = true;
                bop1_2 = bop1_1 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop1_3 = bop4_1 =
                        bop4_2 = bop4_3 = false;
                break;


            case R.id.op4_1:
                bop4_1 = true;
                bop1_2 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop1_1 =
                        bop4_2 = bop4_3 = false;
                break;
            case R.id.op4_2:
                bop4_2 = true;
                bop1_1 = bop1_3 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop1_2 = bop4_3 = false;
                break;
            case R.id.op4_3:
                bop4_3 = true;
                bop1_2 = bop1_1 = bop2_1 = bop2_2 = bop2_3 = bop3_1 = bop3_2 = bop3_3 = bop4_1 =
                        bop4_2 = bop1_3 = false;
                break;
        }
        strdata = "";
        popupEscolha();
    }

    public void popupEscolha() {

        LayoutInflater factory = LayoutInflater.from(this);
        View view = factory.inflate(
                R.layout.montar, null);

        setarSpinnerCategoria(view);
        setarSpinnerCor(view);
        setarDatePicker(view);
        setarTimePicker(view);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(view);


        view.findViewById(R.id.btVoltarMontar).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!strdata.equals("")) {
                    alertDialog.dismiss();
                    String saida = "";
                    String categoria;
                    if (booleanH) {
                        saida += "Hidratação\n";
                        categoria = "Hidratação";
                    } else if (booleanN) {
                        saida += "Nutrição\n";
                        categoria = "Nutrição";
                    } else if (booleanR) {
                        saida += "Reconstrução\n";
                        categoria = "Reconstrução";
                    } else {
                        corEscolhida = Color.rgb(255, 255, 255);
                        saida += "Nada\n";
                        categoria = "Nada";
                    }

                    setTextColor(saida, categoria);
                    setAlarm();
                } else {
                    Toast.makeText(MonteCronograma.this, "Escolha uma data!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alertDialog.show();

    }

    private void setAlarm() {

    }

    private void setTextColor(String saida, String categoria) {

        String clique = null;
        if (bop1_1) {
            op1_1.setText(saida + strdata);
            op1_1.setBackgroundColor(corEscolhida);
            clique = "op11";
        } else if (bop1_2) {
            op1_2.setText(saida + strdata);
            op1_2.setBackgroundColor(corEscolhida);
            clique = "op12";
        } else if (bop1_3) {
            op1_3.setText(saida + strdata);
            op1_3.setBackgroundColor(corEscolhida);
            clique = "op13";
        } else if (bop2_1) {
            op2_1.setText(saida + strdata);
            op2_1.setBackgroundColor(corEscolhida);
            clique = "op21";
        } else if (bop2_2) {
            op2_2.setText(saida + strdata);
            op2_2.setBackgroundColor(corEscolhida);
            clique = "op22";
        } else if (bop2_3) {
            op2_3.setText(saida + strdata);
            op2_3.setBackgroundColor(corEscolhida);
            clique = "op23";
        } else if (bop3_1) {
            clique = "op31";
            op3_1.setText(saida + strdata);
            op3_1.setBackgroundColor(corEscolhida);
        } else if (bop3_2) {
            clique = "op32";
            op3_2.setText(saida + strdata);
            op3_2.setBackgroundColor(corEscolhida);
        } else if (bop3_3) {
            clique = "op33";
            op3_3.setText(saida + strdata);
            op3_3.setBackgroundColor(corEscolhida);
        } else if (bop4_1) {
            clique = "op41";
            op4_1.setText(saida + strdata);
            op4_1.setBackgroundColor(corEscolhida);
        } else if (bop4_2) {
            clique = "op42";
            op4_2.setText(saida + strdata);
            op4_2.setBackgroundColor(corEscolhida);
        } else if (bop4_3) {
            clique = "op43";
            op4_3.setText(saida + strdata);
            op4_3.setBackgroundColor(corEscolhida);
        }
        //String clique, String categoria, String cor, Date dia
        ModeloCronograma modeloCronograma = new ModeloCronograma(clique, categoria, corEscolhida, stringToDate(strdata));

        BD bd = new BD(this);
        long inserir = bd.inserir(modeloCronograma);
        Toast.makeText(this, inserir + "", Toast.LENGTH_SHORT).show();
        bd.desconectar();
    }

    private Date stringToDate(String dia) {
        Log.i("DIA ANTES", dia);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(dia);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.i("DIA DEPOIS", date + "");
        return date;
    }

    private void setarSpinnerCor(View view) {
        // azul, verde, amarelho, rosa
        // Spinner element
        spinnerCor = (Spinner) view.findViewById(R.id.spinnerCor);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Azul");
        categories.add("Laranja");
        categories.add("Rosa");
        categories.add("Verde");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerCor.setAdapter(dataAdapter);

        spinnerCor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();

                int azul = Color.rgb(47, 117, 181);
                int laranja = Color.rgb(237, 125, 49);
                int rosa = Color.rgb(255, 0, 128);
                int verde = Color.rgb(0, 255, 0);

                switch (item) {
                    case "Azul":
                        corEscolhida = azul;
                        break;
                    case "Laranja":
                        corEscolhida = laranja;
                        break;
                    case "Rosa":
                        corEscolhida = rosa;
                        break;
                    case "Verde":
                        corEscolhida = verde;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();

                int azul = Color.rgb(47, 117, 181);
                int laranja = Color.rgb(237, 125, 49);
                int rosa = Color.rgb(255, 0, 128);
                int verde = Color.rgb(0, 255, 0);

                switch (item) {
                    case "Azul":
                        corEscolhida = azul;
                        break;
                    case "Laranja":
                        corEscolhida = laranja;
                        break;
                    case "Rosa":
                        corEscolhida = rosa;
                        break;
                    case "Verde":
                        corEscolhida = verde;
                        break;
                }
            }
        });*/
    }

    private void setarSpinnerCategoria(View view) {
        // Spinner element
        Spinner spinner = (Spinner) view.findViewById(R.id.spinnerCategoria);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Hidratação");
        categories.add("Nutrição");
        categories.add("Reconstrução");
        categories.add("Nada");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        tvcor = (TextView) view.findViewById(R.id.tvcor);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();

                switch (item) {
                    case "Hidratação":
                        booleanH = true;
                        booleanN = false;
                        booleanR = false;
                        spinnerCor.setVisibility(View.VISIBLE);
                        tvcor.setVisibility(View.VISIBLE);
                        break;
                    case "Nutrição":
                        booleanH = false;
                        booleanN = true;
                        booleanR = false;
                        spinnerCor.setVisibility(View.VISIBLE);
                        tvcor.setVisibility(View.VISIBLE);
                        break;
                    case "Reconstrução":
                        booleanH = false;
                        booleanN = false;
                        booleanR = true;
                        spinnerCor.setVisibility(View.VISIBLE);
                        tvcor.setVisibility(View.VISIBLE);
                        break;
                    case "Nada":
                        booleanH = false;
                        booleanN = false;
                        booleanR = false;
                        spinnerCor.setVisibility(View.GONE);
                        tvcor.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setarTimePicker(View view) {
        hora = (TextView) view.findViewById(R.id.btHora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimeField();
            }
        });
    }

    private void setTimeField() {

        int timeHour = 0, timeMinute = 0;
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String strhora, strminuto;
                if (hourOfDay < 10) {
                    strhora = "0" + hourOfDay;
                } else {
                    strhora = String.valueOf(hourOfDay);
                }
                if (minute < 10) {
                    strminuto = "0" + minute;
                } else {
                    strminuto = String.valueOf(minute);
                }
                hora.setText(strhora + " : " + strminuto);
                strhorario = strhora + " : " + strminuto;
            }
        }, timeHour, timeMinute, DateFormat.is24HourFormat(this));

        timePickerDialog.show();
    }

    private void setarDatePicker(View view) {

        data = (TextView) view.findViewById(R.id.btData);
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateTimeField();
            }
        });
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        data.setText(dateFormatter.format(newDate.getTime()));
                        strdata = dateFormatter.format(newDate.getTime());
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }
}

