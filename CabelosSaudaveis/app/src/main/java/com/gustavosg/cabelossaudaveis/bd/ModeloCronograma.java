package com.gustavosg.cabelossaudaveis.bd;


import java.util.Date;

/**
 * Projeto: CabelosSaudaveis
 * Criado por Gustavo em 29/12/2016.
 */
public class ModeloCronograma {
    // clique string, categoria string, dia string, cor string
    private String clique, categoria;
    private int cor;
    private Date dia;

    public ModeloCronograma() {
    }

    public ModeloCronograma(String clique, String categoria, int cor, Date dia) {
        this.clique = clique;
        this.categoria = categoria;
        this.cor = cor;
        this.dia = dia;
    }

    public String getClique() {
        return clique;
    }

    public void setClique(String clique) {
        this.clique = clique;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getCor() {
        return cor;
    }

    public void setCor(int cor) {
        this.cor = cor;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }
}

