package com.gustavosg.cabelossaudaveis.tab;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.gustavosg.cabelossaudaveis.R;
import com.gustavosg.cabelossaudaveis.Util;


public class LFragment extends Fragment implements View.OnClickListener {
    public static final String comp1L = "componente1L";
    public static final String comp2L = "componente2L";
    public static final String comp3L = "componente3L";
    public CheckBox componente1;
    public CheckBox componente2;
    public CheckBox componente3;
    private SharedPreferences spc1L;
    private SharedPreferences spc2L;
    private SharedPreferences spc3L;

    private SharedPreferences.Editor ec1L;
    private SharedPreferences.Editor ec2L;
    private SharedPreferences.Editor ec3L;

    private boolean clicou = false;

    private View view;

    public LFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_l,
                container, false);

        setarCheckBox();

        return view;
    }

    private void setarCheckBox() {
        componente1 = (CheckBox) view.findViewById(R.id.componente1_L);
        componente2 = (CheckBox) view.findViewById(R.id.componente2_L);
        componente3 = (CheckBox) view.findViewById(R.id.componente3_L);
    }

    private void setarSP() {
        spc1L = getActivity().getSharedPreferences(comp1L, 0);
        spc2L = getActivity().getSharedPreferences(comp2L, 0);
        spc3L = getActivity().getSharedPreferences(comp3L, 0);

        ec1L = spc1L.edit();
        ec2L = spc2L.edit();
        ec3L = spc3L.edit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setarSP();

        componente1.setOnClickListener(this);
        componente2.setOnClickListener(this);
        componente3.setOnClickListener(this);

        if (!clicou) {
            new MyTask().execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Util().clearSharedPreferences(getContext());
    }

    @Override
    public void onClick(View view) {
        clicou = true;
        switch (view.getId()) {
            case R.id.componente1_L:
                ec1L.putBoolean("estado1L", componente1.isChecked());
                ec1L.commit();
                break;
            case R.id.componente2_L:
                ec2L.putBoolean("estado2L", componente2.isChecked());
                ec2L.commit();
                break;
            case R.id.componente3_L:
                ec3L.putBoolean("estado3L", componente3.isChecked());
                ec3L.commit();
                break;
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ec1L.putBoolean("estado1L", false);
            ec1L.commit();

            ec2L.putBoolean("estado2L", false);
            ec2L.commit();

            ec3L.putBoolean("estado3L", false);
            ec3L.commit();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }
}