package com.gustavosg.firebaseanalytics.Control;

import com.gustavosg.firebaseanalytics.BD.BD;
import com.gustavosg.firebaseanalytics.Model.Carro;

import java.util.ArrayList;

/**
 * Projeto: MVC
 * Criado por Gustavo em 06/02/2017.
 */
public class MainController {

    private CarroController carroController;

    public MainController(BD bd) {

        carroController = new CarroController(bd);
    }

    public String inserirCarro(String nome, String marca, int id) {
        String saida = "";
        if (carroController.inserirCarro(new Carro(nome, marca, id)) != -1) {
            saida = "Carro inserido com sucesso!";
        } else {
            saida = "ERRO! Tente novamente inserir o carro!";
        }
        return saida;
    }

    public boolean existeCarroCadastrado() {
        return carroController.existeCarroCadastrado();
    }

    public ArrayList<Carro> carrosCadastrados() {
        return carroController.carrosCadastrados();
    }
}
