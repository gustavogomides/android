package com.gustavosg.firebaseanalytics;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.gustavosg.firebaseanalytics.BD.BD;
import com.gustavosg.firebaseanalytics.Control.MainController;
import com.gustavosg.firebaseanalytics.Model.Carro;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private MainController mainController;

    private EditText nome, marca;
    private Button adicionar, atualizar;
    private TextView carrosCadastrados;

    private BD bd;
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        bd = new BD(this);
        mainController = new MainController(bd);

        nome = (EditText) findViewById(R.id.nome);
        marca = (EditText) findViewById(R.id.marca);
        adicionar = (Button) findViewById(R.id.adicionar);
        atualizar = (Button) findViewById(R.id.atualizar);
        carrosCadastrados = (TextView) findViewById(R.id.carrosCadastrados);

        carrosCadastrados();
    }

    private int randomId() {
        Random rand = new Random();
        int min = 50;
        int max = 1000;
        return rand.nextInt((max - min) + 1) + min;
    }

    @Override
    protected void onStart() {
        super.onStart();

        marca.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean retorno = false;
                if (i == EditorInfo.IME_ACTION_DONE) {
                    cadastrarCarro();
                    retorno = true;
                }
                return retorno;
            }
        });

        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarCarro();
            }
        });

        atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carrosCadastrados();
            }
        });

    }

    private void cadastrarCarro() {
        String n, m;
        n = nome.getText().toString().trim();
        m = marca.getText().toString().trim();
        int id = randomId();

        if (n.equals("") || m.equals("")) {
            Toast.makeText(MainActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this,
                    mainController.inserirCarro(n, m, id), Toast.LENGTH_SHORT).show();
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Cadastrar Carro");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, n);
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }
    }

    private void carrosCadastrados() {
        if (mainController.existeCarroCadastrado()) {
            String saida = "";

            for (Carro c : mainController.carrosCadastrados()) {
                saida += c.getNome() + " - " + c.getMarca() + " - " + c.getId() + "\n\n";
            }
            Bundle bundle = new Bundle();
            bundle.putString("Ação", "Listar Carros");
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            carrosCadastrados.setText(saida);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bd.desconectar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
