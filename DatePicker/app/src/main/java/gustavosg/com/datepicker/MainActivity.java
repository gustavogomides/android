package gustavosg.com.datepicker;

import android.app.ActivityManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView op1_1, op1_2, op1_3;
    private TextView op2_1, op2_2, op2_3;
    private TextView op3_1, op3_2, op3_3;
    private TextView op4_1, op4_2, op4_3;
    private int dateDia;
    private int dateMes;
    private int dateAno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        op1_1 = (TextView) findViewById(R.id.op1_1);
        op1_2 = (TextView) findViewById(R.id.op1_2);
        op1_3 = (TextView) findViewById(R.id.op1_3);

        op2_1 = (TextView) findViewById(R.id.op2_1);
        op2_2 = (TextView) findViewById(R.id.op2_2);
        op2_3 = (TextView) findViewById(R.id.op2_3);

        op3_1 = (TextView) findViewById(R.id.op3_1);
        op3_2 = (TextView) findViewById(R.id.op3_2);
        op3_3 = (TextView) findViewById(R.id.op3_3);

        op4_1 = (TextView) findViewById(R.id.op4_1);
        op4_2 = (TextView) findViewById(R.id.op4_2);
        op4_3 = (TextView) findViewById(R.id.op4_3);
    }

    @Override
    protected void onStart() {
        super.onStart();

        op1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "op1_1", Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putInt(MyConstants.DIA, dateDia);
                bundle.putInt(MyConstants.MES, dateMes);
                bundle.putInt(MyConstants.ANO, dateAno);
                DatePickerFragment datePickerFragment = new DatePickerFragment(new MyHandler());
                datePickerFragment.setArguments(bundle);


                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(datePickerFragment, MyConstants.DATE_PICKER);
                transaction.commit();
            }
        });
    }

    class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            dateDia = bundle.getInt(MyConstants.DIA);
            dateMes = bundle.getInt(MyConstants.MES);
            dateAno = bundle.getInt(MyConstants.ANO);

            String data = dateDia + "/" + dateMes + "/" + dateAno;
            op1_1.setText(data);
        }
    }
}
