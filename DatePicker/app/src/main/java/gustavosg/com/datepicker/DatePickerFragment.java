package gustavosg.com.datepicker;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private int dateDia;
    private int dateMes;
    private int dateAno;
    private Handler handler;

    public DatePickerFragment(Handler handler) {
        this.handler = handler;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        dateDia = bundle.getInt(MyConstants.DIA);
        dateMes = bundle.getInt(MyConstants.MES);
        dateAno = bundle.getInt(MyConstants.ANO);

        return new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK,
                this, dateAno, dateMes, dateDia);
    }

    @Override
    public void onDateSet(DatePicker view, int ano, int mes, int dia) {
        dateDia = dia;
        dateMes = mes;
        dateAno = ano;
        Bundle b = new Bundle();
        b.putInt(MyConstants.DIA, dateDia);
        b.putInt(MyConstants.MES, dateMes);
        b.putInt(MyConstants.ANO, dateAno);
        Message msg = new Message();
        msg.setData(b);
        handler.sendMessage(msg);
    }
}