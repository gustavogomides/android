package gustavosg.com.datepicker;

public abstract class MyConstants {
    public static final String DIA = "date_dia";
    public static final String MES = "date_mes";
    public static final String ANO = "date_ano";
    public static final String DATE_PICKER = "date_picker";
}
