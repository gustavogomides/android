package gustavosg.com.datepickerdialog;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private TextView op1_1, op1_2, op1_3;
    private TextView op2_1, op2_2, op2_3;
    private TextView op3_1, op3_2, op3_3;
    private TextView op4_1, op4_2, op4_3;
    private TextView data;
    private TextView hora;
    private String strdata;
    private String strhora;
    private boolean booleanH = true, booleanN = false, booleanR = false, booleanNA = false;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        op1_1 = (TextView) view.findViewById(R.id.op1_1);
        op1_2 = (TextView) view.findViewById(R.id.op1_2);
        op1_3 = (TextView) view.findViewById(R.id.op1_3);

        op2_1 = (TextView) view.findViewById(R.id.op2_1);
        op2_2 = (TextView) view.findViewById(R.id.op2_2);
        op2_3 = (TextView) view.findViewById(R.id.op2_3);

        op3_1 = (TextView) view.findViewById(R.id.op3_1);
        op3_2 = (TextView) view.findViewById(R.id.op3_2);
        op3_3 = (TextView) view.findViewById(R.id.op3_3);

        op4_1 = (TextView) view.findViewById(R.id.op4_1);
        op4_2 = (TextView) view.findViewById(R.id.op4_2);
        op4_3 = (TextView) view.findViewById(R.id.op4_3);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        op1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoria();
            }
        });
    }

    public void categoria() {

        LayoutInflater factory = LayoutInflater.from(getActivity());
        View view = factory.inflate(
                R.layout.montar, null);

        setarSpinner(view);
        setarDatePicker(view);
        setarTimePicker(view);

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setView(view);


        view.findViewById(R.id.btVoltarMontar).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String saida = "";

                if (booleanH) {
                    saida += "Hidratação\n";
                } else if (booleanN) {
                    saida += "Nutrição\n";
                } else if (booleanR) {
                    saida += "Reconstrução\n";
                } else {
                    saida += "Nada\n";
                }
                op1_1.setText(saida + strdata);
                /*if (booleancat1) {
                    cat1.setText(strcat1);
                } else if (booleancat2) {
                    cat2.setText(strcat2);
                } else if (booleancat3) {
                    cat3.setText(strcat3);
                }
                setColor();*/
            }
        });

        alertDialog.show();

    }

    private void setarSpinner(View view) {
        // Spinner element
        Spinner spinner = (Spinner) view.findViewById(R.id.spinnerCategoria);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Hidratação");
        categories.add("Nutrição");
        categories.add("Reconstrução");
        categories.add("Nada");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();

                if (item.equals("Hidratação")) {
                    booleanH = true;
                    booleanN = false;
                    booleanR = false;
                    booleanNA = false;
                } else if (item.equals("Nutrição")) {
                    booleanH = false;
                    booleanN = true;
                    booleanR = false;
                    booleanNA = false;
                } else if (item.equals("Reconstrução")) {
                    booleanH = false;
                    booleanN = false;
                    booleanR = true;
                    booleanNA = false;
                } else if (item.equals("Nada")) {
                    booleanH = false;
                    booleanN = false;
                    booleanR = false;
                    booleanNA = true;
                }


                /*if (booleancat1) {
                    strcat1 += item;
                    strcat11 = item;
                } else if (booleancat2) {
                    strcat2 += item;
                    strcat22 = item;
                } else if (booleancat3) {
                    strcat3 += item;
                    strcat33 = item;
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setarTimePicker(View view) {
        hora = (TextView) view.findViewById(R.id.btHora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimeField();
            }
        });
    }

    private void setTimeField() {

        int timeHour = 0, timeMinute = 0;
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                hora.setText(hourOfDay + " : " + minute);
                strhora = hourOfDay + " : " + minute;
            }
        }, timeHour, timeMinute, DateFormat.is24HourFormat(getActivity()));

        timePickerDialog.show();
    }

    private void setarDatePicker(View view) {

        data = (TextView) view.findViewById(R.id.btData);
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateTimeField();
            }
        });
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);


                /*if (booleancat1) {
                    strcat1 += " - " + dateFormatter.format(newDate.getTime());
                } else if (booleancat2) {
                    strcat2 += " - " + dateFormatter.format(newDate.getTime());
                } else if (booleancat3) {
                    strcat3 += " - " + dateFormatter.format(newDate.getTime());
                }*/
                        data.setText(dateFormatter.format(newDate.getTime()));
                        strdata = dateFormatter.format(newDate.getTime());
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }

}
