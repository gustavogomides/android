package com.gustavosg.androidmysql;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class CadastroActivity extends AppCompatActivity {

    private Button btnSalvar;
    private EditText nome;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        nome = (EditText) findViewById(R.id.editTextnome);
        email = (EditText) findViewById(R.id.editTextemail);

        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    StringBuilder strURL = new StringBuilder();
                    strURL.append("http://192.168.0.102/inserir.php?");
                    strURL.append("nome=");
                    strURL.append(URLEncoder.encode(nome.getText().toString(), "UTF-8"));
                    strURL.append("&email=");
                    strURL.append(URLEncoder.encode(email.getText().toString(), "UTF-8"));
                    new HttpRequest().execute(strURL.toString());
                } catch (Exception ex) {

                }

                /*
                try {
                    StringBuilder strURL = new StringBuilder();
                    strURL.append("http://192.168.25.7/inserir.php?");
                    strURL.append("nome=");
                    strURL.append(URLEncoder.encode(txtNome.getText().toString(), "UTF-8"));
                    strURL.append("&email=");
                    strURL.append(URLEncoder.encode(txtEmail.getText().toString(), "UTF-8"));
                    new HttpRequest().execute(strURL.toString());
                } catch(Exception ex){

                }
                */

            }
        });
    }


    private class HttpRequest extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String retorno = null;
            try {
                String urlHttp = params[0];
                URL url = new URL(urlHttp);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                InputStreamReader ips = new InputStreamReader(http.getInputStream());
                BufferedReader bfr = new BufferedReader(ips);
                retorno = bfr.readLine();

            } catch (Exception ex) {

            }
            return retorno;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("YES")) {
                Toast.makeText(getBaseContext(), "Cliente cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext(), ListarActivity.class));
            } else {
                Toast.makeText(getBaseContext(), "Erro ao cadastrar o cliente!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
