package com.gustavosg.poupanca;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class BD {
    private SQLiteDatabase bd;
    private BDCore auxBd;

    public BD(Context context) {
        auxBd = new BDCore(context);
        bd = auxBd.getWritableDatabase();
    }

    public void desconectar() {
        SQLiteDatabase db = auxBd.getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    public long inserirPoupanca(Poupanca poupanca) {
        ContentValues valores = new ContentValues();
        valores.put("nome", poupanca.getNome());
        valores.put("valor", poupanca.getValor());
        valores.put("data", poupanca.getData());

        return bd.insert("poupanca", null, valores);
    }

    public ArrayList<Poupanca> poupancasCadastradas() {
        ArrayList<Poupanca> arrayListPoupanca = new ArrayList<>();

        String sql = "SELECT * from poupanca";
        Cursor cursor = bd.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            return null;
        } else if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                arrayListPoupanca.add(new Poupanca(cursor.getString(0), cursor.getDouble(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arrayListPoupanca;
    }

    public boolean existePoupancaCadastrada() {
        boolean existe;

        String sql = "SELECT * from poupanca";
        Cursor cursor = bd.rawQuery(sql, null);

        existe = cursor.getCount() != 0;

        cursor.close();
        return existe;
    }

    public List<Poupanca> poupancasCadastradasList() {
        List<Poupanca> arrayListPoupanca = new ArrayList<>();

        String sql = "SELECT * from poupanca order by (nome)";
        Cursor cursor = bd.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            return null;
        } else if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                arrayListPoupanca.add(new Poupanca(cursor.getString(0), cursor.getDouble(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arrayListPoupanca;
    }

    public boolean deletarPoupanca(String nome) {
        String table = "poupanca";
        String where = "nome = '" + nome + "'";
        Log.i("NOME", nome);
        return bd.delete(table, where, null) != 0;
    }

    public long atualizarPoupanca(String nome, double valor) {
        ContentValues valores = new ContentValues();
        valores.put("valor", valor);
        return bd.update("poupanca", valores, "nome=?", new String[]{nome});
    }
}

