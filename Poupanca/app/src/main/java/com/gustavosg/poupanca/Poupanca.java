package com.gustavosg.poupanca;

/**
 * Projeto: Poupanca
 * Criado por Gustavo em 31/01/2017.
 */
public class Poupanca {

    private String nome;
    private double valor;
    private String data;

    public Poupanca() {
    }

    public Poupanca(String nome, double valor, String data) {
        this.nome = nome;
        this.valor = valor;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
