package com.gustavosg.poupanca;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDCore extends SQLiteOpenHelper {
    private static final String NOME_DB = "poupancaBD";
    private static final int VERSAO_BD = 1;

    public BDCore(Context context) {
        super(context, NOME_DB, null, VERSAO_BD);
    }


    @Override
    public void onCreate(SQLiteDatabase bd) {
        bd.execSQL("create table poupanca(nome varchar(100), valor double, data varchar(100), primary key(nome));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int oldVersion, int newVersion) {
        onCreate(bd);
    }
}

