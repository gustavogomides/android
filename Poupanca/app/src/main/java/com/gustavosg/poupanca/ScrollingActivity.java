package com.gustavosg.poupanca;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {
    private FloatingActionButton adicionarDieta;
    private BD bd;
    private PoupancaAdapter poupancaAdapter;
    private List<Poupanca> listPoupancas;
    //private TextView teste;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adicionarDieta = (FloatingActionButton) findViewById(R.id.adicionarDieta);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        bd = new BD(this);

        if (bd.existePoupancaCadastrada()) {
            listPoupancas = bd.poupancasCadastradasList();
        } else {
            listPoupancas = new ArrayList<>();
        }

        poupancaAdapter = new PoupancaAdapter(this, listPoupancas, bd);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(poupancaAdapter);

        poupancaAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onStart() {
        super.onStart();

        adicionarDieta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adicionarPoupanca();
            }
        });
    }

    private void adicionarPoupanca() {
        LayoutInflater factory = LayoutInflater.from(this);
        View view = factory.inflate(R.layout.cadastrar_poupanca, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(view);

        TextView titulo = (TextView) view.findViewById(R.id.alertaTitulo);
        final EditText nomeET = (EditText) view.findViewById(R.id.alertaNome);
        final EditText valorET = (EditText) view.findViewById(R.id.alertaValor);
        Button salvar = (Button) view.findViewById(R.id.btAlerta);

        titulo.setText(this.getResources().getString(R.string.adicionar_poupanca));
        salvar.setText(this.getResources().getString(R.string.salvar));

        valorET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean retorno = false;
                if (i == EditorInfo.IME_ACTION_DONE) {
                    addPoupanca(nomeET, valorET, alertDialog);
                    retorno = true;
                }
                return retorno;
            }
        });

        salvar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPoupanca(nomeET, valorET, alertDialog);
            }
        });

        alertDialog.show();
    }

    private void addPoupanca(EditText nomeET, EditText valorET, AlertDialog alertDialog) {
        String nome = nomeET.getText().toString().trim();
        String valor = valorET.getText().toString().trim();

        if (nome.equals("")) {
            Toast.makeText(ScrollingActivity.this, "Digite o nome da poupança!", Toast.LENGTH_SHORT).show();
        } else if (valor.equals("")) {
            Toast.makeText(ScrollingActivity.this, "Digite o valor (pode ser zero)!", Toast.LENGTH_SHORT).show();
        } else {
            ArrayList<Poupanca> poupancas = bd.poupancasCadastradas();
            boolean jaExiste = false;

            if (poupancas != null) {
                for (Poupanca p : poupancas) {
                    if (p.getNome().toLowerCase().equals(nome.toLowerCase())) {
                        jaExiste = true;
                        break;
                    }
                }
            }

            if (!jaExiste) {
                long date = System.currentTimeMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String data = sdf.format(date);
                Poupanca p = new Poupanca(nome, Double.parseDouble(valor), data);
                if (bd.inserirPoupanca(p) != -1) {
                    Toast.makeText(ScrollingActivity.this, "Poupança criada com sucesso!", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                    listPoupancas.add(p);
                    Collections.sort(listPoupancas, new Comparator<Poupanca>() {
                        @Override
                        public int compare(Poupanca o1, Poupanca o2) {
                            return o1.getNome().toLowerCase().compareTo(o2.getNome().toLowerCase());
                        }
                    });
                    poupancaAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(ScrollingActivity.this, "ERRO! Tente novamente criar a poupança!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ScrollingActivity.this, "Poupança para " + nome + " já foi criada anteriormente!",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bd.desconectar();
    }

    private boolean ehInteiro(double valor) {
        return valor == (int) valor;
    }
}
