package com.gustavosg.poupanca;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Projeto: Poupanca
 * Criado por Gustavo em 31/01/2017.
 */
public class PoupancaAdapter extends RecyclerView.Adapter<PoupancaAdapter.MyViewHolder> {

    private Context mContext;
    private List<Poupanca> poupancas;
    private AlertDialog alerta;
    private BD bd;

    public PoupancaAdapter(Context mContext, List<Poupanca> poupancas, BD bd) {
        this.mContext = mContext;
        this.poupancas = poupancas;
        this.bd = bd;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.poupanca_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Poupanca poupanca = poupancas.get(position);
        final String nome;
        final double val;
        nome = poupanca.getNome();
        val = poupanca.getValor();
        final String valor;
        if (val == (int) val) {
            valor = String.valueOf((int) val);
        } else {
            valor = String.valueOf(val);
        }

        String strNome = "Poupança para: " + nome;
        holder.titulo.setText(strNome);

        String strValor = "R$ " + valor;
        holder.valor.setText(strValor);

        String strData = "Início: " + poupanca.getData();
        holder.data.setText(strData);

        holder.atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertaAtualizar(nome, valor, holder.getAdapterPosition());
            }
        });

        holder.remover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertaRemover(nome, holder.getAdapterPosition());
            }
        });

    }


    private void alertaAtualizar(final String nome, final String valor, final int position) {
        LayoutInflater factory = LayoutInflater.from(mContext);
        View view = factory.inflate(R.layout.atualizar_poupanca, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setView(view);

        TextView titulo = (TextView) view.findViewById(R.id.alertaTitulo);
        final EditText nomeET = (EditText) view.findViewById(R.id.alertaNome);
        nomeET.setEnabled(false);
        nomeET.setText(nome);

        final EditText valorET = (EditText) view.findViewById(R.id.alertaValor);
        valorET.setText(valor);

        Button atualizar = (Button) view.findViewById(R.id.btAlerta);

        titulo.setText(mContext.getResources().getString(R.string.atualizar_poupanca));
        atualizar.setText(mContext.getResources().getString(R.string.atualizar));

        final EditText novoValorET = (EditText) view.findViewById(R.id.novoValor);
        novoValorET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean retorno = false;
                if (i == EditorInfo.IME_ACTION_DONE) {
                    update(nome, valor, novoValorET.getText().toString(), alertDialog, position);
                    retorno = true;
                }
                return retorno;
            }
        });

        atualizar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                update(nome, valor, novoValorET.getText().toString(), alertDialog, position);
            }
        });

        alertDialog.show();
    }

    private void update(String nome, String valAntigo, String valAtual, AlertDialog alertDialog, int position) {
        double valorAntigo = Double.parseDouble(valAntigo);
        double atualizar = Double.parseDouble(valAtual);
        double valorNovo = valorAntigo + atualizar;

        if (bd.atualizarPoupanca(nome, valorNovo) != -1) {
            alertDialog.dismiss();
            updateAt(position, nome, valorNovo);
        } else {
            Toast.makeText(mContext, "ERRO! Tente novamente atualizar!", Toast.LENGTH_SHORT).show();
        }
    }

    private void alertaRemover(final String nome, final int position) {
        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        //define o titulo
        builder.setTitle("Remover Poupança");
        //define a mensagem
        builder.setMessage("Poupança para: " + nome + " será removida. Tem certeza?");
        builder.setCancelable(false);

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                alerta.dismiss();
            }
        });

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (bd.deletarPoupanca(nome)) {
                    removeAt(position);
                }
            }
        });

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }

    @Override
    public int getItemCount() {
        return poupancas.size();
    }


    public void updateAt(int position, String nome, double valorNovo) {
        int posicao = 0;

        for (int i = 0; i < poupancas.size(); i++) {
            if (poupancas.get(i).getNome().toLowerCase().equals(nome)) {
                posicao = i;
                break;
            }
        }

        poupancas.get(posicao).setValor(valorNovo);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        poupancas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeRemoved(position, poupancas.size());
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titulo, valor, atualizar, remover, data;
        public CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            valor = (TextView) itemView.findViewById(R.id.valor);
            data = (TextView) itemView.findViewById(R.id.data);
            atualizar = (TextView) itemView.findViewById(R.id.atualizar);
            remover = (TextView) itemView.findViewById(R.id.remover);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
