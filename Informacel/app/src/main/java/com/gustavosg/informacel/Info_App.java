package com.gustavosg.informacel;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Info_App extends Fragment implements OnItemClickListener {

    private PackageManager packageManager;
    private ListView apkList;
    private Handler handler = new Handler();
    private ProgressDialog mDialog;
    private View view;
    private TextView nomeTV;
    private TextView pacoteTV;
    private TextView versionTV;
    private TextView instalacaoTV;
    private TextView modificacaoTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.app_main,
                container, false);

        apkList = (ListView) view.findViewById(R.id.applist);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mDialog = ProgressDialog.show(getActivity(), "Aguarde", "Obtendo Informações...", true);
        new Thread() {
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        packageManager = getActivity().getPackageManager();
                        List<PackageInfo> packageList = packageManager
                                .getInstalledPackages(PackageManager.GET_PERMISSIONS);

                        List<PackageInfo> packageList1 = new ArrayList<PackageInfo>();

                        /*To filter out System apps*/
                        for (PackageInfo pi : packageList) {
                            boolean b = isSystemPackage(pi);
                            if (!b) {
                                packageList1.add(pi);
                            }
                        }
                        apkList.setAdapter(new AppAdapter(getActivity(), packageList1, packageManager));
                    }
                });
            }
        }.start();
        mDialog.dismiss();
        apkList.setOnItemClickListener(this);
    }


    private boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
                : false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long row) {


        PackageInfo packageInfo = (PackageInfo) parent.getItemAtPosition(position);

        informacoesApp(packageInfo);

    }

    public void informacoesApp(final PackageInfo packageInfo) {

        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

        final LinearLayout popupView = (LinearLayout) layoutInflater.inflate(R.layout.popup_window_app, null);

        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        setarTV(popupView, packageInfo);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setAnimationStyle(R.style.PopupWindow);
        popupWindow.setBackgroundDrawable(view.getContext().getResources().getDrawable(android.R.color.transparent));
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        Button btnDismiss = (Button) popupView.findViewById(R.id.btVoltar);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        Button btDesinstalar = (Button) popupView.findViewById(R.id.button_desinstalar);
        btDesinstalar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                String pacote = "package:" + packageInfo.packageName;
                intent.setData(Uri.parse(pacote));
                startActivity(intent);
            }
        });

        Button btAbrir = (Button) popupView.findViewById(R.id.button_abrir);
        btAbrir.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(packageInfo.packageName);
                startActivity(launchIntent);
            }
        });

    }

    public void setarTV(LinearLayout popupView, PackageInfo packageInfo) {

        PackageManager packageManager = getActivity().getPackageManager();

        String appName = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
        nomeTV = (TextView) popupView.findViewById(R.id.tvTitleApp);
        nomeTV.setText(appName);

        String packageName = packageInfo.packageName;
        pacoteTV = (TextView) popupView.findViewById(R.id.app_pacote);
        pacoteTV.setText(packageName);

        String versionName = packageInfo.versionName;
        versionTV = (TextView) popupView.findViewById(R.id.app_versao);
        versionTV.setText(versionName);

        String first_installation = setDateFormat(packageInfo.firstInstallTime);
        instalacaoTV = (TextView) popupView.findViewById(R.id.app_instalado);
        instalacaoTV.setText(first_installation);

        String last_modified = setDateFormat(packageInfo.lastUpdateTime);
        modificacaoTV = (TextView) popupView.findViewById(R.id.app_modificado);
        modificacaoTV.setText(last_modified);

    }

    @SuppressLint("SimpleDateFormat")
    private String setDateFormat(long time) {
        Date date = new Date(time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        String strDate = formatter.format(date);
        return strDate;
    }
}