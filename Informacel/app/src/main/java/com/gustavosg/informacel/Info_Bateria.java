package com.gustavosg.informacel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;

import java.text.DecimalFormat;

public class Info_Bateria extends Fragment {

    final String appKey = "4242229d287af83c2f7615523ece5f5fadcc3cbc1863c275";
    private TextView noBateria;
    private TextView nivelBateria;
    private TextView tecnologiaBateria;
    private TextView conectadoBateria;
    private TextView saudeBateria;
    private TextView statusBateria;
    private TextView voltagemBateria;
    private TextView temperaturaBateria;
    private ProgressBar progressBar;
    private BroadcastReceiver battery_receiver;
    private ImageView tomada;
    private ImageView usb;
    private ImageView wireless;
    private ImageView descarregando;
    private TextView percentageBattery;
    private Handler handler = new Handler();

    //Informações Bateria e Internet
    private void getBatteryPercentage() {
        battery_receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isPresent = intent.getBooleanExtra("present", false);
                String technology = intent.getStringExtra("technology");
                int plugged = intent.getIntExtra("plugged", -1);
                int scale = intent.getIntExtra("scale", -1);
                int health = intent.getIntExtra("health", 0);
                int status = intent.getIntExtra("status", 0);
                int rawlevel = intent.getIntExtra("level", -1);
                double voltage = intent.getIntExtra("voltage", 0);
                double temperature = intent.getIntExtra("temperature", 0);
                double temperatureF;
                int level = 0;

                DecimalFormat dec = new DecimalFormat("##.##");

                if (isPresent) {
                    if (rawlevel >= 0 && scale > 0) {
                        level = (rawlevel * 100) / scale;
                    }
                    //progressBar.setProgress(level);
                    ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, 0, level);
                    anim.setDuration(1200);
                    progressBar.startAnimation(anim);
                    String porcentagem = level + "%";
                    percentageBattery.setText(porcentagem);

                    temperatureF = ((temperature / 10) * 1.8) + 32;

                    String v = dec.format(voltage / 1000) + "V";
                    String volt = v.replace(',', '.');

                    String t = dec.format((temperature / 10)) + "ºC / " + dec.format(temperatureF) + "ºF";
                    String temp = t.replace(',', '.');

                    nivelBateria.setText(level + "%");
                    tecnologiaBateria.setText(technology);
                    conectadoBateria.setText(getPlugTypeString(plugged));
                    saudeBateria.setText(getHealthString(health));
                    statusBateria.setText(getStatusString(status));
                    voltagemBateria.setText(volt);
                    temperaturaBateria.setText(temp);

                } else {
                    noBateria.setText("Bateria não encontrada!!!");
                }

            }
        };

        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        getActivity().registerReceiver(battery_receiver, batteryLevelFilter);
    }

    // Tipo Carregador da bateria
    private String getPlugTypeString(int plugged) {
        String plugType = "Não";
        switch (plugged) {
            case BatteryManager.BATTERY_PLUGGED_AC:
                plugType = "AC";
                break;
            case BatteryManager.BATTERY_PLUGGED_USB:
                plugType = "USB";
                break;

            case BatteryManager.BATTERY_PLUGGED_WIRELESS:
                plugType = "Wireless";
                break;
        }
        setarIcones(plugType);
        return plugType;
    }

    public void setarIcones(String tipo) {
        switch (tipo) {
            case "AC":
                tomada.setImageResource(R.drawable.car_tomada_on);
                descarregando.setImageResource(R.drawable.car_bateria);
                usb.setImageResource(R.drawable.car_usb);
                wireless.setImageResource(R.drawable.car_wifi);
                break;

            case "USB":
                usb.setImageResource(R.drawable.car_usb_on);
                wireless.setImageResource(R.drawable.car_wifi);
                tomada.setImageResource(R.drawable.car_tomada);
                descarregando.setImageResource(R.drawable.car_bateria);
                break;

            case "Wireless":
                wireless.setImageResource(R.drawable.car_wifi_on);
                usb.setImageResource(R.drawable.car_usb);
                tomada.setImageResource(R.drawable.car_tomada);
                descarregando.setImageResource(R.drawable.car_bateria);
                break;
            case "Não":
                descarregando.setImageResource(R.drawable.car_bateria_on);
                wireless.setImageResource(R.drawable.car_wifi);
                usb.setImageResource(R.drawable.car_usb);
                tomada.setImageResource(R.drawable.car_tomada);
                break;
        }
    }

    // Saúde da bateria
    private String getHealthString(int health) {
        String healthString = "Desconhecido";
        switch (health) {
            case BatteryManager.BATTERY_HEALTH_DEAD:
                healthString = "Ruim";
                break;
            case BatteryManager.BATTERY_HEALTH_GOOD:
                healthString = "Boa";
                break;
            case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                healthString = "Alta Voltagem";
                break;
            case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                healthString = "Superaquecimento";
                break;
            case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                healthString = "Falha";
                break;

            case BatteryManager.BATTERY_HEALTH_COLD:
                healthString = "Fria";
                break;
        }
        return healthString;
    }

    // Status da bateria
    private String getStatusString(int status) {
        String statusString = "Unknown";
        switch (status) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                statusString = "Carregando";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                statusString = "Descarregando";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                statusString = "Completa";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                statusString = "Não está carregando";
                break;
        }
        return statusString;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bateria_main,
                container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        noBateria = (TextView) view.findViewById(R.id.bateria);
        nivelBateria = (TextView) view.findViewById(R.id.nivel_bateria);
        tecnologiaBateria = (TextView) view.findViewById(R.id.tecnologia_bateria);
        conectadoBateria = (TextView) view.findViewById(R.id.conectado_bateria);
        saudeBateria = (TextView) view.findViewById(R.id.saude_bateria);
        statusBateria = (TextView) view.findViewById(R.id.status_bateria);
        voltagemBateria = (TextView) view.findViewById(R.id.voltagem_bateria);
        temperaturaBateria = (TextView) view.findViewById(R.id.temperatura_bateria);
        percentageBattery = (TextView) view.findViewById(R.id.percentageBaterry);

        tomada = (ImageView) view.findViewById(R.id.tomada);
        usb = (ImageView) view.findViewById(R.id.usb);
        wireless = (ImageView) view.findViewById(R.id.wifi);
        descarregando = (ImageView) view.findViewById(R.id.bateriaDescarregando);

        Appodeal.initialize(getActivity(), appKey, Appodeal.INTERSTITIAL);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        new Thread() {
            public void run() {
                Log.i("LOG:", "Entrou Thread Bateria");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("LOG:", "Entrou Handler Bateria");
                        // INFORMAÇÕES BATERIA
                        getBatteryPercentage();


                    }
                });
            }
        }.start();
    }


    @Override
    public void onResume() {
        super.onResume();
        //Appodeal.onResume(getActivity(), Appodeal.INTERSTITIAL);
        Log.d("Ciclo", "Fragment: Metodo onResume() chamado");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Appodeal.setTesting(true);

        //Appodeal.initialize(getActivity(), appKey, Appodeal.INTERSTITIAL);
        Appodeal.show(getActivity(), Appodeal.INTERSTITIAL);

        Appodeal.show(getActivity(), Appodeal.SKIPPABLE_VIDEO);

    }
}
