package com.gustavosg.informacel;

import android.app.ActivityManager;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.appodeal.ads.Appodeal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Info_Memory extends Fragment {

    final String appKey = "4242229d287af83c2f7615523ece5f5fadcc3cbc1863c275";
    ListView memoryList;
    private Handler handler = new Handler();

    public static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    public static long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    public static long getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return availableBlocks * blockSize;
        } else {
            return 0;
        }
    }

    public static long getTotalExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return totalBlocks * blockSize;
        } else {
            return 0;
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = " KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
                if (size >= 1024) {
                    suffix = " GB";
                    size /= 1024;
                }
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, '.');
            commaOffset -= 3;
        }
        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.memory_main,
                container, false);

        memoryList = (ListView) view.findViewById(R.id.list_of_memory);

        Appodeal.initialize(getActivity(), appKey, Appodeal.INTERSTITIAL);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        new Thread() {
            public void run() {
                Log.i("LOG: ", "Entrou Thread Memória");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("LOG: ", "Entrou Handler Memória");
                        List<Memory> memoryDataSource = new ArrayList<Memory>();

                        String heading = "Memória RAM";
                        long totalRamValue = totalRamMemorySize();
                        long freeRamValue = freeRamMemorySize();
                        long usedRamValue = totalRamValue - freeRamValue;
                        int imageIcon = R.drawable.piechart;

                        Memory mMemory = new Memory(heading, formatSize(usedRamValue), formatSize(freeRamValue), formatSize(totalRamValue), imageIcon);
                        memoryDataSource.add(mMemory);

                        String internalMemoryTitle = "Memória Interna";
                        long totalInternalValue = getTotalInternalMemorySize();
                        long freeInternalValue = getAvailableInternalMemorySize();
                        long usedInternalValue = totalInternalValue - freeInternalValue;
                        int internalIcon = R.drawable.piecharttwo;

                        Memory internalMemory = new Memory(internalMemoryTitle, formatSize(usedInternalValue), formatSize(freeInternalValue), formatSize(totalInternalValue), internalIcon);
                        memoryDataSource.add(internalMemory);

                        String externalMemoryTitle = "Memória Externa";
                        long totalExternalValue = getTotalExternalMemorySize();
                        long freeExternalValue = getAvailableExternalMemorySize();
                        long usedExternalValue = totalExternalValue - freeExternalValue;
                        int externalIcon = R.drawable.piechartone;

                        Memory externalMemory = new Memory(externalMemoryTitle, formatSize(usedExternalValue), formatSize(freeExternalValue), formatSize(totalExternalValue), externalIcon);
                        memoryDataSource.add(externalMemory);

                        MemoryAdapter memoryAdapter = new MemoryAdapter(getActivity(), memoryDataSource);
                        memoryList.setAdapter(memoryAdapter);

                        Appodeal.show(getActivity(), Appodeal.INTERSTITIAL);

                    }
                });
            }
        }.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Ciclo", "Fragment: Metodo onResume() chamado");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public long freeRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getActivity().getApplicationContext().getSystemService(getActivity().ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem;

        return availableMegs;
    }

    public long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getActivity().getApplicationContext().getSystemService(getActivity().ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem;
        return availableMegs;
    }

}
