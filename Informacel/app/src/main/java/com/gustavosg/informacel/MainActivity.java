package com.gustavosg.informacel;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getFragmentManager();
        if (id == R.id.nav_home) { // HOME
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
        } else if (id == R.id.nav_bateria) { // BATERIA
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Info_Bateria()).commit();
        } else if (id == R.id.nav_android) { // ANDROID
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Info_Android()).commit();
        } else if (id == R.id.nav_bluetooth) { // BLUETOOTH
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Info_Bluetooth()).commit();
        } else if (id == R.id.nav_memory) { // MEMÓRIA
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Info_Memory()).commit();
        } else if (id == R.id.nav_app) { // INFORMAÇÕES
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Info_App()).commit();
        } else if (id == R.id.nav_about_app) { // INFORMAÇÃO
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Information()).commit();
        } else if (id == R.id.playstore) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (id == R.id.nav_duvida) { // DUVIDAS
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object;
            final String COMPARTILHAR = getResources().getString(R.string.textocompartilhar) + " http://market.android.com/details?id=" + appPackageName;
            final String SHARE = getResources().getString(R.string.enviarvia);
            final String SUBJECT = getResources().getString(R.string.subject);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT);
            emailIntent.putExtra(Intent.EXTRA_TEXT, COMPARTILHAR);
            startActivity(Intent.createChooser(emailIntent, SHARE + "\n"));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
