package com.gustavosg.informacel;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Info_Bluetooth extends Fragment {

    Context context;
    String switchOn = "Ativado";
    String switchOff = "Desativado";
    private TextView nameBluetooth;
    private TextView macBluetooth;
    private TextView nameBluetoothTitle;
    private TextView macBluetoothTitle;
    private Switch mySwitch;
    private BluetoothAdapter mBluetoothAdapter;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.bluetooth_main,
                container, false);

        nameBluetooth = (TextView) view.findViewById(R.id.nome_bluetooth_list);
        macBluetooth = (TextView) view.findViewById(R.id.mac_bluetooth_list);
        nameBluetoothTitle = (TextView) view.findViewById(R.id.title_name_bluetooth);
        macBluetoothTitle = (TextView) view.findViewById(R.id.title_mac);
        mySwitch = (Switch) view.findViewById(R.id.switch1);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // INFORMAÇÕES BLUETOOTH
        getBluetooth();

    }


    public void getBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final String address = mBluetoothAdapter.getAddress();
        final String nome = mBluetoothAdapter.getName();
        boolean enable = mBluetoothAdapter.isEnabled();

        if (!enable) {
            mySwitch.setText(switchOff);
            Toast.makeText(getActivity().getApplicationContext(), "Bluetooth Desativado", Toast.LENGTH_SHORT).show();
            nameBluetooth.setText("");
            macBluetooth.setText("");
            nameBluetoothTitle.setText("");
            macBluetoothTitle.setText("");
        } else {

            mySwitch.setText(switchOn);
            mySwitch.setChecked(true);
            Toast.makeText(getActivity().getApplicationContext(), "Bluetooth Ativado", Toast.LENGTH_SHORT).show();
            nameBluetooth.setText(nome);
            macBluetooth.setText(address);
            nameBluetoothTitle.setText("Nome:");
            macBluetoothTitle.setText("Endereço MAC:");

        }

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    mySwitch.setText(switchOn);
                    mBluetoothAdapter.enable();
                    Toast.makeText(getActivity().getApplicationContext(), "Ativando Bluetooth...", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity().getApplicationContext(), "Bluetooth Ativado", Toast.LENGTH_SHORT).show();

                    nameBluetooth.setText(nome);
                    macBluetooth.setText(address);
                    nameBluetoothTitle.setText("Nome:");
                    macBluetoothTitle.setText("Endereço MAC:");
                } else {
                    mySwitch.setText(switchOff);
                    mBluetoothAdapter.disable();

                    Toast.makeText(getActivity().getApplicationContext(), "Bluetooth Desativado", Toast.LENGTH_SHORT).show();

                    nameBluetooth.setText("");
                    macBluetooth.setText("");
                    nameBluetoothTitle.setText("");
                    macBluetoothTitle.setText("");
                }
            }
        });
    }
}
