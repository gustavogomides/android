package com.gustavosg.informacel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                finish();

                Intent intent = new Intent();
                intent.setClass(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }, 6000);

        RunAnimation();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void RunAnimation() {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.scale);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale);
        a.reset();
        anim.reset();

        TextView tv = (TextView) findViewById(R.id.textSplash);
        ImageView img = (ImageView) findViewById(R.id.imageViewSplash);

        tv.startAnimation(a);
        img.startAnimation(anim);

    }
}
