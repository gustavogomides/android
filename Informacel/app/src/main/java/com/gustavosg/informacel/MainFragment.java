package com.gustavosg.informacel;


import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class MainFragment extends Fragment {

    private TextView mainModelo;
    private TextView mainFabricante;
    private TextView mainPais;
    private Handler handler = new Handler();

    public MainFragment() {
    }

    public void getInfoCel() {
        new Thread() {
            public void run() {
                Log.i("LOG: ", "Entrou thread");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("LOG: ", "Entrou handler");
                        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                        String manufacturer = Build.MANUFACTURER;
                        String model = Build.MODEL;
                        String fabricante = manufacturer.substring(0, 1).toUpperCase() + manufacturer.substring(1);
                        String networkCountry = telephonyManager.getNetworkCountryIso();
                        String pais = networkCountry.substring(0).toUpperCase();

                        if (pais.equals("")) {
                            mainModelo.setText(model);
                            mainFabricante.setText(fabricante);

                        } else {
                            mainModelo.setText(model);
                            mainFabricante.setText(fabricante);
                            mainPais.setText(pais);
                        }
                    }
                });
            }
        }.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mainModelo = (TextView) rootView.findViewById(R.id.main_modelo);
        mainFabricante = (TextView) rootView.findViewById(R.id.main_fabricante);
        mainPais = (TextView) rootView.findViewById(R.id.main_pais);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        // INFORMAÇÕES BATERIA
        getInfoCel();
    }
}


