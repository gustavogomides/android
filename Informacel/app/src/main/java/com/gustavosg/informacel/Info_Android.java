package com.gustavosg.informacel;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;

public class Info_Android extends Fragment {

    final String appKey = "4242229d287af83c2f7615523ece5f5fadcc3cbc1863c275";
    private Handler handler = new Handler();
    private ImageView imagem_android_release;
    private TextView nomeTV;
    private TextView sdkTV;
    private TextView versaoTV;
    private Button btnOpenPopup;
    private View view;
    private TextView bootloaderTV;
    private TextView bootloaderTV_title;
    private TextView hardwareTV;
    private TextView hardwareTV_title;
    private TextView numVersaoTV;
    private TextView numVersaoTV_title;
    private TextView serialTV;
    private TextView serialTV_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.android_main,
                container, false);

        imagem_android_release = (ImageView) view.findViewById(R.id.imagem_android_release);
        nomeTV = (TextView) view.findViewById(R.id.android_nome);
        versaoTV = (TextView) view.findViewById(R.id.android_release);

        // SDK
        sdkTV = (TextView) view.findViewById(R.id.android_sdk);

        btnOpenPopup = (Button) view.findViewById(R.id.button_android);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Ciclo", "Fragment: Metodo onStart() chamado");

        new Thread() {
            public void run() {
                Log.i("Log: ", "Entrou thread Android");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("Log: ", "Entrou handler Android");

                        String release = Build.VERSION.RELEASE;
                        String sdk = "" + Build.VERSION.SDK_INT;

                        if (sdk.equals("11") || sdk.equals("12") || sdk.equals("13")) {
                            Log.i("ANDROID", "HoneyComb");
                            imagem_android_release.setImageResource(R.drawable.honeycomb);
                            nomeTV.setText("HoneyComb");

                        } else if (sdk.equals("14") || sdk.equals("15")) {
                            Log.i("ANDROID", "Ice Cream Sandwich");
                            imagem_android_release.setImageResource(R.drawable.icecreamsandwich);
                            nomeTV.setText("Ice Cream Sandwich");

                        } else if (sdk.equals("16") || sdk.equals("17") || sdk.equals("18")) {
                            Log.i("ANDROID", "Jelly Bean");
                            imagem_android_release.setImageResource(R.drawable.jellybean);
                            nomeTV.setText("Jelly Bean");

                        } else if (sdk.equals("19") || sdk.equals("20")) {
                            Log.i("ANDROID", "KitKat");
                            imagem_android_release.setImageResource(R.drawable.kitkat);
                            nomeTV.setText("KitKat");

                        } else if (sdk.equals("21") || sdk.equals("22")) {
                            Log.i("ANDROID", "Lollipop");
                            imagem_android_release.setImageResource(R.drawable.lollipop);
                            nomeTV.setText("Lollipop");

                        } else if (sdk.equals("23")) {
                            Log.i("ANDROID", "Marshmallow");
                            imagem_android_release.setImageResource(R.drawable.marshmallow);
                            nomeTV.setText("Marshmallow");
                        }

                        versaoTV.setText(release);
                        sdkTV.setText(sdk);


                    }
                });
            }
        }.start();


        btnOpenPopup.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Appodeal.initialize(getActivity(), appKey, Appodeal.INTERSTITIAL);
                Appodeal.show(getActivity(), Appodeal.INTERSTITIAL);

                informacoesAvancadas();

            }
        });
    }

    public void informacoesAvancadas() {

        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

        LinearLayout popupView = (LinearLayout) layoutInflater.inflate(R.layout.popup_window_android, null);

        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        setarTV(popupView);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setAnimationStyle(R.style.PopupWindow);
        popupWindow.setBackgroundDrawable(view.getContext().getResources().getDrawable(android.R.color.transparent));
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        Button btnDismiss = (Button) popupView.findViewById(R.id.btOk);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

    }

    public void setarTV(LinearLayout popupView) {

        TextView tv = (TextView) popupView.findViewById(R.id.tvTitle);
        String title = "Informações Avançadas";
        tv.setText(title);

        // BOOTLOADER
        bootloaderTV = (TextView) popupView.findViewById(R.id.android_bootloader);
        bootloaderTV_title = (TextView) popupView.findViewById(R.id.android_bootloader_id);

        // HARDWARE
        hardwareTV = (TextView) popupView.findViewById(R.id.android_hardware);
        hardwareTV_title = (TextView) popupView.findViewById(R.id.android_hardware_id);

        // NUM VERSAO
        numVersaoTV = (TextView) popupView.findViewById(R.id.android_numversao);
        numVersaoTV_title = (TextView) popupView.findViewById(R.id.android_numversao_id);

        // SERIAL
        serialTV = (TextView) popupView.findViewById(R.id.android_serial);
        serialTV_title = (TextView) popupView.findViewById(R.id.android_serial_id);


        String boot = Build.BOOTLOADER;
        String bootloader;
        if (boot.equals("unknown")) {
            bootloader = "Desconhecido";
        } else {
            bootloader = boot;
        }
        bootloaderTV_title.setText("BootLoader:");
        bootloaderTV.setText(bootloader);

        String hardware = Build.HARDWARE;
        hardwareTV_title.setText("Hardware:");
        hardwareTV.setText(hardware);

        String id = Build.ID;
        numVersaoTV_title.setText("Número da Versão:");
        numVersaoTV.setText(id);

        String serial = Build.SERIAL;
        serialTV_title.setText("Serial:");
        serialTV.setText(serial);
    }


}



